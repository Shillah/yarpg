#pragma once

#ifdef __cplusplus
#include <cstdint>
#include <cstddef>
#include <cassert>
#else
#include <stdint.h>
#include <stddef.h>
#include <assert.h>
#endif



#define YARPG_ASSERT(Expr) assert(Expr)
#define FWDDECLARE(Type) struct Type; typedef struct Type Type

// memory stuff
typedef size_t   memory_index;
typedef char     byte;

#define PTROFFSET(Ptr, Offset) (typeof(Ptr)) ((byte *) (Ptr) + (Offset))
#define NEXTALIGNED(Ptr, Alignment) (void *) (((memory_index) (Ptr) + (Alignment) - 1) \
					      & ~((memory_index)(Alignment) - 1))

#define ARRAYCOUNT(Array) (sizeof(Array)/sizeof(*Array))

// unsigned int
typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

// signed int
typedef int8_t   i8;
typedef int16_t  i16;
typedef int32_t  i32;
typedef int64_t  i64;

// bool
typedef i8    b8;
typedef i16   b16;
typedef i32   b32;
typedef i64   b64;

// float
typedef float    f32;
typedef double   f64;

struct signed_vector_2
{
	i32 X, Y;
};

struct signed_vector_3
{
	i32 X, Y, Z;
};

struct signed_vector_4
{
	i32 X, Y, Z, W;
};

typedef struct signed_vector_2 iv2;
typedef struct signed_vector_3 iv3;
typedef struct signed_vector_4 iv4;
#define IV2(ArgX, ArgY) (iv2) {.X = (ArgX), .Y = (ArgY)}
#define IV3(ArgX, ArgY, ArgZ) (iv3) {.X = (ArgX), .Y = (ArgY), .Z = (ArgZ)}
#define IV4(ArgX, ArgY, ArgZ, ArgW) (iv4) {.X = (ArgX), .Y = (ArgY), .Z = (ArgZ), .W = (ArgW)}
#define IV_DISPATCH(_1,_2,_3,_4, NAME, ...) NAME
#define IV(args...) IV_DISPATCH(args, IV4, IV3, IV2)(args)

struct unsigned_vector_2
{
	u32 X, Y;
};

struct unsigned_vector_3
{
	u32 X, Y, Z;
};

struct unsigned_vector_4
{
	u32 X, Y, Z, W;
};

typedef struct unsigned_vector_2 uv2;
typedef struct unsigned_vector_3 uv3;
typedef struct unsigned_vector_4 uv4;

#define UV2(ArgX, ArgY) (uv2) {.X = (ArgX), .Y = (ArgY)}
#define UV3(ArgX, ArgY, ArgZ) (uv3) {.X = (ArgX), .Y = (ArgY), .Z = (ArgZ)}
#define UV4(ArgX, ArgY, ArgZ, ArgW) (uv4) {.X = (ArgX), .Y = (ArgY), .Z = (ArgZ), .W = (ArgW)}
#define UV_DISPATCH(_1,_2,_3,_4, NAME, ...) NAME
#define UV(args...) UV_DISPATCH(args, UV4, UV3, UV2)(args)

struct vector_2
{
	f32 X, Y;
};

struct vector_3
{
	f32 X, Y, Z;
};

struct vector_4
{
	f32 X, Y, Z, W;        
};

typedef struct vector_2 v2;
typedef struct vector_3 v3;
typedef struct vector_4 v4;

typedef struct
{
	union {
		v2 TopRight;
                struct {
			f32 MaxX, MaxY;
		};
	};
	union {
		v2 BottomLeft;
		struct {
			f32 MinX, MinY;
		};
	};
        
} rect;

typedef struct
{
	union {
		iv2 TopRight;
                struct {
			i32 MaxX, MaxY;
		};
	};
	union {
		iv2 BottomLeft;
		struct {
			i32 MinX, MinY;
		};
	};
} irect;

#define V2(ArgX, ArgY) (v2) {.X = (ArgX), .Y = (ArgY)}
#define V3(ArgX, ArgY, ArgZ) (v3) {.X = (ArgX), .Y = (ArgY), .Z = (ArgZ)}
#define V4(ArgX, ArgY, ArgZ, ArgW) (v4) {.X = (ArgX), .Y = (ArgY), .Z = (ArgZ), .W = (ArgW)}
#define V_DISPATCH(_1,_2,_3,_4, NAME, ...) NAME
#define V(args...) V_DISPATCH(args, V4, V3, V2)(args)

#define kilo_bytes(value) ((value) * 1024)                       // fits into u32
#define mega_bytes(value) (kilo_bytes(value) * 1024)             // fits into u32
#define giga_bytes(value) (mega_bytes((u64)(value)) * 1024)   // might not fit into u32,
                                                                 // so we cast it to u64
#define tera_bytes(value) (giga_bytes((u64)(value)) * 1024)

#define KB * ((u64) 1024)
#define MB * ((u64) 1024 KB)
#define GB * ((u64) 1024 MB)
#define TB * ((u64) 1024 GB)

#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN
#endif

EXTERN rect RectOfMWH(v2 Middle, f32 Width, f32 Height);
EXTERN rect RectOfTB(v2 TopLeft, v2 BottomRight);
EXTERN rect RectOfBounds(f32 Bottom, f32 Top, f32 Left, f32 Right);

#ifdef DEBUG
#define DURING_DEBUG(xs...) xs
#else
#define DURING_DEBUG(xs...)
#endif

#ifdef __cplusplus
////////////////////////////////////////////////////////////////////////////////
// V2 Operations

b32 operator==(v2 A, v2 B);
b32 operator!=(v2 A, v2 B);
v2 operator+(v2 A, v2 B);
v2 operator-(v2 A);
v2 operator-(v2 A, v2 B);
v2 operator*(v2 A, f32 B);
v2 operator*(f32 B, v2 A);
f32 operator*(v2 A, v2 B);

////////////////////////////////////////////////////////////////////////////////
// V3 Operations

b32 operator==(v3 A, v3 B);
b32 operator!=(v3 A, v3 B);
v3 operator+(v3 A, v3 B);
v3 operator-(v3 A);
v3 operator-(v3 A, v3 B);
v3 operator*(v3 A, f32 B);
v3 operator*(f32 B, v3 A);
f32 operator*(v3 A, v3 B);

////////////////////////////////////////////////////////////////////////////////
// V4 Operations

b32 operator==(v4 A, v4 B);
b32 operator!=(v4 A, v4 B);
v4 operator+(v4 A, v4 B);
v4 operator-(v4 A);
v4 operator-(v4 A, v4 B);
v4 operator*(v4 A, f32 B);
v4 operator*(f32 B, v4 A);
f32 operator*(v4 A, v4 B);

////////////////////////////////////////////////////////////////////////////////
// IV2 Operations

b32 operator==(iv2 A, iv2 B);
b32 operator!=(iv2 A, iv2 B);
iv2 operator+(iv2 A, iv2 B);
iv2 operator-(iv2 A);
iv2 operator-(iv2 A, iv2 B);
iv2 operator*(iv2 A, i32 B);
iv2 operator*(i32 B, iv2 A);
i32 operator*(iv2 A, iv2 B);

////////////////////////////////////////////////////////////////////////////////
// IV3 Operations

b32 operator==(iv3 A, iv3 B);
b32 operator!=(iv3 A, iv3 B);
iv3 operator+(iv3 A, iv3 B);
iv3 operator-(iv3 A);
iv3 operator-(iv3 A, iv3 B);
iv3 operator*(iv3 A, i32 B);
iv3 operator*(i32 B, iv3 A);
i32 operator*(iv3 A, iv3 B);

////////////////////////////////////////////////////////////////////////////////
// IV4 Operations

b32 operator==(iv4 A, iv4 B);
b32 operator!=(iv4 A, iv4 B);
iv4 operator+(iv4 A, iv4 B);
iv4 operator-(iv4 A);
iv4 operator-(iv4 A, iv4 B);
iv4 operator*(iv4 A, i32 B);
iv4 operator*(i32 B, iv4 A);
i32 operator*(iv4 A, iv4 B);

////////////////////////////////////////////////////////////////////////////////
// UV2 Operations

b32 operator==(uv2 A, uv2 B);
b32 operator!=(uv2 A, uv2 B);
uv2 operator+(uv2 A, uv2 B);
uv2 operator-(uv2 A);
uv2 operator-(uv2 A, uv2 B);
uv2 operator*(uv2 A, u32 B);
uv2 operator*(u32 B, uv2 A);
u32 operator*(uv2 A, uv2 B);

////////////////////////////////////////////////////////////////////////////////
// UV3 Operations

b32 operator==(uv3 A, uv3 B);
b32 operator!=(uv3 A, uv3 B);
uv3 operator+(uv3 A, uv3 B);
uv3 operator-(uv3 A);
uv3 operator-(uv3 A, uv3 B);
uv3 operator*(uv3 A, u32 B);
uv3 operator*(u32 B, uv3 A);
u32 operator*(uv3 A, uv3 B);

////////////////////////////////////////////////////////////////////////////////
// UV4 Operations

b32 operator==(uv4 A, uv4 B);
b32 operator!=(uv4 A, uv4 B);
uv4 operator+(uv4 A, uv4 B);
uv4 operator-(uv4 A);
uv4 operator-(uv4 A, uv4 B);
uv4 operator*(uv4 A, u32 B);
uv4 operator*(u32 B, uv4 A);
u32 operator*(uv4 A, uv4 B);


////////////////////////////////////////////////////////////////////////////////
// RECT Operactions

rect operator+(rect Rect, v2 Offset);
rect operator-(rect Rect, v2 Offset);
rect operator*(rect Rect, f32 factor);
rect operator+(v2 Offset, rect Rect);
rect operator-(v2 Offset, rect Rect);
rect operator*(f32 factor, rect Rect);

////////////////////////////////////////////////////////////////////////////////
// IRECT Operactions

irect operator+(irect Rect, iv2 Offset);
irect operator-(irect Rect, iv2 Offset);
irect operator*(irect Rect, i32 factor);
irect operator+(iv2 Offset, irect Rect);
irect operator-(iv2 Offset, irect Rect);
irect operator*(i32 factor, irect Rect);

#endif

#define SYMBOL(X) #X
#define STRING(X) SYMBOL(X)
