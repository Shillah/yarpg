#pragma once

#include <def.h>

enum list_allocator_flags
{
	ListAllocator_SENTINEL = 0x1,
	ListAllocator_USED = 0x2,
};

struct memory_block 
{
	struct memory_block *Prev, *Next;
	memory_index Size;
	b32 Flags;        
};

struct list_allocator
{        
	struct memory_block Sentinel;
};

#ifndef __cplusplus
typedef struct memory_block memory_block;
typedef struct list_allocator list_allocator;
#endif

b32  InitializeListAllocator(list_allocator *Alloc, memory_index Size, void *Memory);

void *SlowAlloc(list_allocator *Alloc, memory_index Size);
void *SlowRealloc(list_allocator *Alloc, memory_index Size, void *Old);
void *SlowArrayAlloc(list_allocator *Alloc, u32 Count, memory_index Size);

void SlowFree(void *ToFree);
