#pragma once

struct header 
{
	u32 header_size;
	u8 num_entries;
} __attribute__((packed));

typedef struct header header;
	
struct asset_handle
{
	FILE *file_handle;
	u16 *entry_names;
	u64 *entry_sizes;
	u64 *entry_offsets;
	char *name_data;
	header h;
};

struct asset_manager
{
        u32 NumHandles;
	asset_handle *Handles;
};

typedef struct asset_handle asset_handle;

#define MAX_ASSET_NAME_LENGTH (1 << (8 * sizeof(u16)))
#define ASSET_HANDLE_SIZE (sizeof(asset_handle) - sizeof(header))
