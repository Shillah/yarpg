#pragma once

#include <stdio.h>

#include <def.h>

EXTERN FILE *open_file(char const *filename);
EXTERN void close_file(FILE *file_to_close);
EXTERN u64 read_file(
	FILE *file_to_read,
	u64 offset_into_file,
	u64 number_of_bytes_to_read,
	char *buffer // has to be at least `number_of_bytes_to_read` big
	);
