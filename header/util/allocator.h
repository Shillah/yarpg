#pragma once

typedef void *(*memory_allocate)(struct allocator *Alloc, memory_index Size);
typedef void *(*memory_free)(struct allocator *Alloc, void *ToFree);

struct allocator
{        
	memory_allocate Allocate;
	memory_free     Free;
};

#ifndef __cplusplus
typedef struct allocator allocator;
#endif
