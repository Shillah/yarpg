#pragma once

#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN
#endif

struct performance_res
{
	long precision;
};

struct performance_result
{
	long real_time;
	long cycles;
};

#ifndef __cplusplus
typedef struct performance_res performance_res;
typedef struct performance_result performance_result;
#endif

EXTERN performance_res   performance_stat(void);
EXTERN performance_result performance_query(void);


