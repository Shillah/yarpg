#pragma once

#include <def.h>

struct memory_arena
{        
	byte *Base;
	memory_index Size, Used;
};

#ifndef __cplusplus
typedef struct memory_arena memory_arena;
#endif

#ifdef __cplusplus
#define ALIGNMENT(Type) (alignof(Type))
#else
#define ALIGNMENT(Type) (_Alignof(Type))
#endif

EXTERN void InitializeArena(memory_arena *Arena, void *Base, memory_index Size);
EXTERN void *PushSize(memory_arena *Arena, memory_index Size);
#define PushStruct(Arena, Struct) ((Struct *) PushSize((Arena), sizeof(Struct)))
#define PushArray(Arena, Num, Type) ((Type *) PushSize((Arena), (Num) * sizeof(Type)))
EXTERN void *PushSizeAligned(memory_arena *Arena, memory_index Size, u32 Alignment);
#define PushAlignedStruct(Arena, Struct) \
	((Struct *) PushSizeAligned((Arena), sizeof(Struct), ALIGNMENT(Struct)))
#define PushAlignedArray(Arena, Num, Type) \
	((Type *) PushSizeAligned((Arena), (Num) * sizeof(Type), ALIGNMENT(Type)))
EXTERN void FreeAll(memory_arena *Arena);
