#pragma once

#include <def.h>
#include <opengl/opengl.h>
#include <platform.h>
#include <nuklear.h>

struct mesh
{
	GLuint num_indices, num_vertices;
	float  *vertices, *vertex_colors;
	GLuint *indices;
};

struct gpu_mesh
{
#define VERTEX_DATA 0
#define COLOR_DATA  1
#define INDEX_DATA  2
	GLuint vao, vbo[3], num_indices;
	GLenum draw_mode;
};

struct platform
{
	nuklear_context Ctx;
};

// Returns 0 if initialized successfully
/* #define GAME_INIT(name) EXTERN i32 name(game_memory *mem) */
/* typedef GAME_INIT(game_init); */
/* game_init GameInit; */

#define GAME_INIT(name)  i32 name(game_memory *Memory)
typedef GAME_INIT(game_init);
#define GAME_UPDATE_AND_RENDER(name)  void name(platform *Platform, game_memory *Memory, game_input Input)
typedef GAME_UPDATE_AND_RENDER(game_update_and_render);
#define GAME_RELEASE(name)  void name(game_memory *Memory)
typedef GAME_RELEASE(game_release);
#define GAME_KEY_HANDLER(name)  void name(int key, int mode, game_memory *mem)
typedef GAME_KEY_HANDLER(game_key_handler);
