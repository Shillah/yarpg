#pragma once

#include <def.h>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

EXTERN void check_errors(char const *prefix);

#define GL_QUICK_CHECK() check_errors(__FILE__ ":" STRING(__LINE__))
