#pragma once

#include <def.h>
#include <opengl/opengl.h>
#include <opengl/shader.h>
#include <opengl/renderer.h>

#define NUM_ASCII_CHARS 256
#define MAX_EXTRA_GLYPHS 4096

struct tex_coords
{
	// MinY is assumed to be 0.0
	f32 MaxY, MinX, MaxX;
};

#ifndef __cplusplus
typedef struct tex_coords tex_coords;
#endif

struct ascii_map
{
	tex_coords TexCoords[NUM_ASCII_CHARS];
	i32 Size[NUM_ASCII_CHARS][2];
	i32 Bearing[NUM_ASCII_CHARS][2];
	u32 Advance[NUM_ASCII_CHARS];
};

struct unicode_map
{
	// input hashmap u64 (glyph index) -> glyph data
	// with backing array of size MAX_EXTRA_GLYPHS
	// should try open hashmap?
	i32 __NOTHING__;
};

struct string_state {
	GLuint vao, vbo;
        program StringProgram;
	struct
	{
		GLuint texture;
		u32 pt_size;

		// height: distance between 2 baselines (i.e. 2 lines)
		// ascender:  max height _over_  baseline
		// descender: max height _under_ baseline
		u16 ascender, descender, height;

		u16 _texturew, _textureh;

		f32 _texcoords[256][3];
		i32 size[256][2];
		i32 bearing[256][2];
		u32 advance[256];

		//meseared in 64ths of a pixel
	} glyph_map;
};

#ifndef __cplusplus
typedef struct string_state string_state;
#endif

EXTERN void string_setup(string_state *buf);
EXTERN void print_string(
	string_state *state,
	f32 x, f32 y, f32 scale, f32 const Color[3],
	u32 num_chars, char const *to_print);
EXTERN void RenderString(string_state *State,
			 renderer     *Renderer,
			 f32 x, f32 y, f32 scale,
			 f32 const Color[3],
			 u32 NumChars, char const *ToPrint);
EXTERN void string_release(string_state *State);

/*
  EXTERN i32 BaseHeight(str)
  EXTERN i32 OverBaseHeight(str)
  EXTERN i32 UnderBaseHeight(str)

  EXTERN i32 Height(str)
  EXTERN i32 Width(str)
 */
