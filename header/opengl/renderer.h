#pragma once

#include <def.h>
#include <opengl/opengl.h>
#include <opengl/shader.h>
#include <util/arena_allocator.h>

// should this be "dynamic"
#define BATCH_VERTEX_SIZE 1024

struct vertex_data
{
	v3 Position;
	v3 Transform;
	v4 Color;
	// ...
};

#ifndef __cplusplus
typedef struct vertex_data vertex_data;
#endif

struct render_batch
{
	u32 Free;
        u32 TexturesUsed;
	struct render_batch *Next;

	// TODO: determine number of textures at runtime
	GLuint      Textures[32];
	u32         Indices[BATCH_VERTEX_SIZE];
	vertex_data Data[BATCH_VERTEX_SIZE];
};

#ifndef __cplusplus
typedef struct render_batch render_batch;
#endif

struct renderer
{
	GLuint StreamVAO, StreamVBO, StreamIBO;
	render_batch *First;
	render_batch *Last;
	memory_arena *Arena;
        program      *Shader;
};

#ifndef __cplusplus
typedef struct renderer renderer;
#endif

EXTERN void RendererCreate(renderer *Renderer, program *Shader);

EXTERN void RendererPrepare(renderer *Renderer, memory_arena *Arena);

EXTERN void DrawTriangles(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data);

EXTERN void DrawTriangleElements(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data,
	u32 NumIndices,
	u32 *Indices);

EXTERN void DrawTriangleStrip(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data);

EXTERN void DrawTriangleFan(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data);

EXTERN void DrawTexturedTriangles(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data,
	GLuint Texture
	);

EXTERN void RendererDraw(renderer *Renderer, GLint ProjectionLocation,
	f32 Projection[]);

EXTERN void RendererRelease(renderer *Renderer);
