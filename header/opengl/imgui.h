#pragma once

#include <def.h>
#include <opengl/renderer.h>
#include <opengl/string.h>

struct layout
{
	i32 Something;
};

struct current_layout
{
	i32 SomethingElse;
};

struct ui_state
{
        i32 FrameHeight, FrameWidth;
	i32 MouseX, MouseY, MouseDown;

	irect ActiveBox, HotBox, CurrentBox;
	b32 ActiveWindow, HotWindow, CurrentWindow;
        b32 ActiveWidget, HotWidget;
	renderer Renderer;
	string_state *StringState;
};

#ifndef __cplusplus
typedef struct ui_state ui_state;
#endif

EXTERN void InitGui(ui_state *UiState,
		    string_state *StringState,
		    program *Shader,
		    i32 FrameHeight, i32 FrameWidth);
EXTERN void PrepareFrame(ui_state *UiState,
			 memory_arena *Arena,
			 i32 MouseX, i32 MouseY, i32 MouseDown);
EXTERN void FinishFrame(ui_state *UiState);
EXTERN b32 Button(ui_state *UiState,
		  i32 Id,
		  i32 x, i32 y,
		  i32 Width, i32 Height);

EXTERN b32 Slider(ui_state *UiState,                  
		  i32 Id, i32 X, i32 Y, i32 Width, i32 Height,
		  f32 *Val, f32 Min, f32 Max);

EXTERN b32 BeginWindow(ui_state *UiState,
		       i32 Id,
		       char const *Title,
		       i32 X, i32 Y, i32 Width, i32 Height, b32 Flags);

EXTERN b32 EndWindow(ui_state *UiState);
