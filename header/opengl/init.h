#pragma once
#include <opengl/opengl.h>

struct opengl
{
	int major, minor, profile, forward_compat, resizable, fullscreen, anti_alias;
};


#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN
typedef struct opengl opengl;
#endif

EXTERN GLFWwindow *init(opengl options, char const *title, int width, int height);

EXTERN void terminate(GLFWwindow *w);
