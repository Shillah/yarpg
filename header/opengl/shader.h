#pragma once

#include <def.h>
#include <opengl/opengl.h>

#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN
#endif

struct program
{
	GLuint Id;
	// per vertex
        GLint  Transform, Position, Color, TexturePos;
	// uniform
	GLint Texture, Projection;
};

#ifndef __cplusplus
typedef struct program program;
#endif

EXTERN program  make_program(
	char const *name,
	char const *vertex_source,
	char const *geometry_source,
	char const *fragment_source);
