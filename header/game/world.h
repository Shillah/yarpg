#pragma once

#include <def.h>

#include <util/arena_allocator.h>
#include <opengl/renderer.h>

// The world is split in chunks and loaded lazily
// it has 3 dimensions

// this is assumed to always be a power of 2
#define MAX_LOADED_CHUNKS 128
#define CHUNK_DIM 9
#define ENTITIES_PER_BLOCK 512
#define MAX_ACTIVE_ENTITIES 4096
#define MAX_LOADED_ENTITIES (8 * 4096)

////////////////////////////////////////////////////////////////////////////////
// forward declerations
////////////////////////////////////////////////////////////////////////////////

FWDDECLARE(world_position);
FWDDECLARE(world_entity);
FWDDECLARE(active_world_entity);
FWDDECLARE(world_chunk);
FWDDECLARE(world_entity_list);
FWDDECLARE(world);

////////////////////////////////////////////////////////////////////////////////
// implementations
////////////////////////////////////////////////////////////////////////////////

typedef enum
{
	EntityType_ERROR,
	EntityType_CITY,
	EntityType_UNIT,
	EntityType_BACKGROUND
} entity_type;

struct world_position
{
	// "address" of the chunk
	iv3 Chunk;
	// offset into the chunk
	v2  Offset;
};

struct world_entity
{
	entity_type    Type;
	world_position Pos;
	rect           BoundingBox;
	f32            Color[3];
};

struct active_world_entity
{
        i32 BaseIndex;
	// Position relative to current Chunk
	v2 RelPosition;
};

struct world_chunk
{
        iv3 Chunk;
	i32 NumEntities;
        i32 EntityIndices[ENTITIES_PER_BLOCK];
	world_chunk *Next;
};

struct world_entity_list
{
	i32 NumEntities;
	world_entity Entities[ENTITIES_PER_BLOCK];
	world_entity_list *Next;
};

struct world
{
	i32 NumLoadedEntities;
	i32 NumActiveEntities;
	world_chunk *LoadedChunks[MAX_LOADED_CHUNKS];
	world_chunk *FreeChunks;
	active_world_entity ActiveEntities[MAX_ACTIVE_ENTITIES];
        world_entity LoadedEntities[MAX_LOADED_ENTITIES];
	memory_arena *Arena;
	world_position CameraPos;
	iv3 ActiveChunk;
};

world_chunk *GetWorldChunk(world *World, iv3 Chunk);
world_chunk *GenerateWorldChunk(world *World, iv3 Chunk, world_chunk *NewChunk);
void InitWorld(world *World, memory_arena *Arena);
void DrawWorld(world *World, renderer *Renderer);
void AddCreature(world *World, world_position Pos, f32 Color[3]);
world_entity *GetEntity(world *World, i32 EntityIndex);
void CanonizePosition(world_position *Position);
void SetCameraPos(world *World, world_position CameraPos);
