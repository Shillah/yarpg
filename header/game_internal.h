#pragma once

#include <game.h>

#include <util/performance.h>
#include <util/file.h>
#include <util/arena_allocator.h>

#include <opengl/string.h>
#include <opengl/imgui.h>

#include <game/world.h>

////////////////////////////////////////////////////////////////////////////////
/// GAME INTERNAL

struct game
{
	memory_arena Arena;
	memory_arena TransientArena;
	gpu_mesh gm, gcursor, gwindow;
	world_position CameraPos;
	i32 CursorX = 0, CursorY = 0;
	program DefaultProgram, WindowProgram;
	u64 precision;
	performance_result flast;
	game_input LastInput;
        f32 ZoomFactor;
	string_state *StringState;        
	f32 CameraMatrix[4][4];
	GLint ProjectionLocation;
	renderer Renderer;
	ui_state Gui;        

        i32 Difficulty;

	world World;
	
	f32 Value;
};
