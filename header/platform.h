#pragma once

// numbers are made up at the moment
#define MINIMUM_TRANSIENT_SIZE 5 MB
#define MINIMUM_PERSISTENT_SIZE 10 MB

struct game_memory
{
	u64 PersistentSize, TransientSize;
	void *PersistentMemory, *TransientMemory;

};

#ifndef __cplusplus
typedef struct game_memory game_memory;
typedef struct platform    platform;
#endif

enum KEYS { KEY_W, KEY_A, KEY_S, KEY_D, NUM_KEYS };
struct game_input
{
	i32 MouseX, MouseY, MouseDown;
	b32 State[NUM_KEYS];
};

#ifndef __cplusplus
typedef struct game_input game_input;
#endif

struct thread_context
{
	void *PlatformData;
};

#ifndef __cplusplus
typedef struct thread_context thread_context;
#endif
