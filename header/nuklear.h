#pragma once

#include <def.h>

#include <opengl/opengl.h>
#include <opengl/shader.h>
#include <util/arena_allocator.h>
#include <util/list_allocator.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#include <Nuklear/nuklear.h>

#include <platform.h>

struct device
{
	struct nk_buffer Commands;
	struct nk_draw_null_texture Nothing;
	GLuint ArrayBuffer;
	union {
		struct {
			GLuint VertexBuffer, IndexBuffer;
		};
		GLuint Buffers[2];
	};
	program Shader;
	GLuint FontTexture;

};

#ifndef __cplusplus
typedef struct device device;
#endif

struct nuklear_context
{
	device              Graphics;
	struct nk_font      *Font;
	struct nk_context   Nuklear;
	list_allocator      ListAlloc;
	memory_arena        ArenaAlloc;
        struct nk_allocator PermAlloc;
	struct nk_allocator TempAlloc;

};

#ifndef __cplusplus
typedef struct nuklear_context nuklear_context;
#endif

EXTERN void InitNuklear(nuklear_context *Ctx, memory_index MemSize, void *Memory);
EXTERN void DrawNuklear(nuklear_context *Ctx, int width, int height,
		 struct nk_vec2 scale, enum nk_anti_aliasing AA);
EXTERN void ReleaseNuklear(nuklear_context *Ctx);
EXTERN void ParseInputsNuklear(nuklear_context *Ctx, game_input *Input);
