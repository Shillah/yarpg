#version 150 core

uniform vec3 offset;
uniform vec3 ucolor;

uniform mat2 transform;

in vec2 position;
in vec3 color;

vec2 u = vec2(1.5, 0.86);
vec2 v = vec2(0.0, 1.73);
mat2 hexagon = mat2(u, v);

out vec3 vert_color;

vec2 stretch(vec2 i)
{
  return vec2(i.x * 2, i.y);
}

void main()
{
  vec2 stretched = stretch(position + hexagon*offset.xy) * offset.z;
  vec2 pos = transform * stretched;
  vert_color = ucolor;
  gl_Position = vec4(pos, 0.0, 1.0);
}
