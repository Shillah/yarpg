#version 150

in vec3 VertexColor;
in vec2 VertexOriginPos;

out vec4 FragmentColor;

float StrokeWidth = 0.1;
float Radius = 0.5;
float Threshold = 1-Radius;
float alpha = 0.75;
vec2 TopLeft = vec2(-1.0, 1.0) * Threshold;
vec2 TopRight = vec2(1.0, 1.0) * Threshold;
vec2 BottomLeft = vec2(-1.0, -1.0) * Threshold;
vec2 BottomRight = vec2(1.0, -1.0) * Threshold;


void main(void)
{
  float EdgeDistance = -1;

  if (VertexOriginPos.x > Threshold &&
      VertexOriginPos.y > Threshold) {
    EdgeDistance = length(VertexOriginPos - TopRight);
    if (EdgeDistance > Radius) discard;
    else if (EdgeDistance > Radius - StrokeWidth) FragmentColor = vec4(vec3(0), 1.0);
    else                                     FragmentColor = vec4(VertexColor, alpha);
  } else if (VertexOriginPos.x < -Threshold &&
	     VertexOriginPos.y > Threshold) {
    EdgeDistance = length(VertexOriginPos - TopLeft);
    if (EdgeDistance > Radius) discard;
    else if (EdgeDistance > Radius - StrokeWidth) FragmentColor = vec4(vec3(0), 1.0);
    else                                     FragmentColor = vec4(VertexColor, alpha);
  } else if (VertexOriginPos.x > Threshold &&
	     VertexOriginPos.y < -Threshold) {
    EdgeDistance = length(VertexOriginPos - BottomRight);
    if (EdgeDistance > Radius) discard;
    else if (EdgeDistance > Radius - StrokeWidth) FragmentColor = vec4(vec3(0), 1.0);
    else                                     FragmentColor = vec4(VertexColor, alpha);
  } else if (VertexOriginPos.x < -Threshold &&
	     VertexOriginPos.y < -Threshold) {
    EdgeDistance = length(VertexOriginPos - BottomLeft);
    if (EdgeDistance > Radius) discard;
    else if (EdgeDistance > Radius - StrokeWidth) FragmentColor = vec4(vec3(0), 1.0);
    else                                     FragmentColor = vec4(VertexColor, alpha);
  } else {
    
    EdgeDistance = 1 - max(abs(VertexOriginPos.x), abs(VertexOriginPos.y));
    if (EdgeDistance < StrokeWidth) FragmentColor = vec4(vec3(0), 1.0);
    else                                FragmentColor = vec4(VertexColor, alpha);
    
  }  
}
