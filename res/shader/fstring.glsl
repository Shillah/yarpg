#version 150 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D text;
uniform vec3 textColor;

void main()
{
  
  float Alpha = texture(text, TexCoords).r;
  
  vec4 Sampled = vec4(1.0, 1.0, 1.0, Alpha);
  // if (Alpha < 0.5)
  // {
  //   Sampled = vec4(0.0, 0.0, 0.0, 0.0);
  // } else if (Alpha < 0.7)
  // {
  //   Sampled = vec4(0.0, 1.0, 0.0, Alpha);
  // } else
  // {
  //   Sampled = vec4(1.0, 1.0, 1.0, Alpha);
  // }
  
  
  color = vec4(textColor, 1.0) * Sampled;
}
