#version 150 core

in vec3 g_color;

out vec4 outColor;

void main()
{
  outColor = vec4(g_color, 1.0);
}
