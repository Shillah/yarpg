#version 150 core
in vec4 Position; // <vec2 pos, vec2 tex>
out vec2 TexCoords;

uniform vec3 transform;

void main()
{
  vec2 TransformedPosition = vec2((Position.x - 500)/ 500, (Position.y - 500)/500);
  gl_Position = vec4(TransformedPosition, 0.0, 1.0);
  TexCoords = Position.zw;
}
