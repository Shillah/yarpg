.PHONY: all clean distclean run install

################################################################################
# We want to change this Makefile a bit
################################################################################
# It should be able to do four things:
# 1) compile the platform code and produce an executable
# 2) compile the game code and produce a shared library
# 3) compile the asset creator and update all assets
# 4) (Optional) statically link the game and the platform code in release mode
# Its probably a good idea to make 3 seperate Makefiles for this.
################################################################################


DEBUG ?= 1
SHARED ?= 0

TITLE=yarpg
LIBRARIES=glew glfw3 freetype2
CC=gcc
CXX=g++
RM=rm -f

COMPILERFLAGS=-Werror -Iheader/ -Icontrib \
-DFONT_NAME="\"contrib/Nuklear/extra_font/Roboto-Regular.ttf\""
ADDCCFLAGS=
#ADDCXXFLAGS= -fno-exceptions -fno-rtti
ADDCXXFLAGS=
LDFLAGS=
LDLIBS= -ldl -lm

# doesnt seem to work for shared libraries
GPROFFLAGS = -pg -fprofile-arcs

SRCDIR=src
DEBUGDIR=debug
RELDIR=release
HDRS= $(wildcard header/**/*.h) $(wildcard header/*.h)

ifeq ($(SHARED), 1)
SRCS=opengl/shader.c util/performance.c opengl/util.c opengl/string.c opengl/renderer.c util/file.c util/arena_allocator.c util/list_allocator.c game/world.c opengl/imgui.cpp def.cpp game.cpp
COMPILERFLAGS += -fPIC 
LDFLAGS += -Wl,--version-script=libyarpg.map -shared
OBJSUFFIX=-shared
TITLE :=lib$(TITLE).so
else
SRCS=opengl/init.c nuklear.c util/arena_allocator.c util/list_allocator.c opengl/util.c opengl/shader.c util/file.c linux_main.cpp
OBJSUFFIX=
endif

LIBRARY_CFLAGS  = $(shell pkg-config --cflags $(LIBRARIES))
LIBRARY_LDFLAGS = $(shell pkg-config --libs-only-L $(LIBRARIES))
LIBRARY_LDLIBS  = $(shell pkg-config --libs-only-l $(LIBRARIES))
LDFLAGS += $(LIBRARY_LDFLAGS)
LDLIBS  += $(LIBRARY_LDLIBS)
COMPILERFLAGS += $(LIBRARY_CFLAGS)

CSRCS = $(filter %.c, $(SRCS))
CXXSRCS = $(filter %.cpp, $(SRCS))
OBJS += $(patsubst %.c,%$(OBJSUFFIX).o,${CSRCS})
OBJS += $(patsubst %.cpp,%$(OBJSUFFIX).o,${CXXSRCS})

ifeq ($(DEBUG), 1)
COMPILERFLAGS += -ggdb -Og -Wall -Wextra \
	-Wno-unused-variable -Wno-unused-function -Wno-unused-but-set-variable \
	-Wno-variadic-macros -Wno-unused-parameter \
	-fsanitize=address,pointer-compare,leak,undefined -fstack-protector \
	-DDEBUG 
LDFLAGS += -fsanitize=address,undefined
OBJDIR=$(DEBUGDIR)
OBJS := $(addprefix $(DEBUGDIR)/, $(OBJS))
RUN_OPTIONS=LSAN_OPTIONS=suppressions=lsan.supp:verbosity=1:print_legend=true
else
OBJDIR=$(RELDIR)
COMPILERFLAGS += -O2 -Wall -DDEBUG -g 	-Wno-unused-variable \
	-Wno-unused-function -Wno-unused-but-set-variable \
	-Wno-variadic-macros -Wno-unused-parameter
OBJS := $(addprefix $(RELDIR)/, $(OBJS))
RUN_OPTIONS=
endif

# See https://stackoverflow.com/questions/52034997/how-to-make-makefile-recompile-when-a-header-file-is-changed
DEPENDS = $(patsubst %.o,%.d,$(OBJS))

RESULT=$(OBJDIR)/$(TITLE)

all: $(RESULT)

-include $(DEPENDS)

CCFLAGS=$(COMPILERFLAGS) $(ADDCCFLAGS)
CXXFLAGS=$(COMPILERFLAGS) $(ADDCXXFLAGS)



$(RESULT): $(OBJS)
	echo $(DEPENDS)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LDLIBS)

# $(OBJDIR)/%.s: $(SRCDIR)/%.cpp
# 	$(CXX) $(CXXFLAGS) -o $@ -S $^

dir_guard=@mkdir -p $(@D)
$(OBJDIR)/%$(OBJSUFFIX).o: $(SRCDIR)/%.cpp Makefile
	$(dir_guard)
	$(CXX) $(CXXFLAGS) -MMD -MP -o $@ -c $<

$(OBJDIR)/%$(OBJSUFFIX).o: $(SRCDIR)/%.c Makefile
	$(dir_guard)
	$(CC) $(CCFLAGS) -MMD -MP -o $@ -c $<

run: all
	$(RUN_OPTIONS) $(addprefix ./, $(RESULT))

test:
	echo $(OBJDIR) $(OBJS)

install:
	mkdir release
	mkdir debug

clean:
	$(RM) $(DEPENDS)
	$(RM) $(OBJS)

distclean: clean
	$(RM) $(TITLE)

asset: src/util/asset.c src/util/file.c
	gcc -DASSET_MAIN -Iheader/ $^ -o $@
