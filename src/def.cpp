#include <def.h>

////////////////////////////////////////////////////////////////////////////////
// V2 Operations

b32 operator==(v2 A, v2 B)
{
	return (A.X == B.X) && (A.Y == B.Y);
}

b32 operator!=(v2 A, v2 B)
{
	return (A.X != B.X) || (A.Y != B.Y);
}

v2 operator+(v2 A, v2 B)
{
	v2 Res = A;
	Res.X += B.X;
	Res.Y += B.Y;
	return Res;
}

v2 operator-(v2 A)
{
	v2 Res = V(-A.X, -A.Y);
	return Res;
}

v2 operator-(v2 A, v2 B)
{
	v2 Res = A + (-B);
	return Res;
}

v2 operator*(v2 A, f32 B)
{
	v2 Res = A;
	Res.X *= B;
	Res.Y *= B;
	return Res;
}

v2 operator*(f32 B, v2 A)
{
	v2 Res = A * B;
	return Res;
}

f32 operator*(v2 A, v2 B)
{
	f32 Res = A.X * B.X + A.Y * B.Y;
	return Res;
}

////////////////////////////////////////////////////////////////////////////////
// V3 Operations

b32 operator==(v3 A, v3 B)
{
	return (A.X == B.X) && (A.Y == B.Y) && (A.Z == B.Z);
}

b32 operator!=(v3 A, v3 B)
{
	return (A.X != B.X) || (A.Y != B.Y) || (A.Z != B.Z);
}

v3 operator+(v3 A, v3 B)
{
	v3 Res = A;
	Res.X += B.X;
	Res.Y += B.Y;
	Res.Z += B.Z;
	return Res;
}

v3 operator-(v3 A)
{
	v3 Res = V(-A.X, -A.Y, -A.Z);
	return Res;
}

v3 operator-(v3 A, v3 B)
{
	v3 Res = A + (-B);
	return Res;
}

v3 operator*(v3 A, f32 B)
{
	v3 Res = A;
	Res.X *= B;
	Res.Y *= B;
	Res.Z *= B;
	return Res;
}

v3 operator*(f32 B, v3 A)
{
	v3 Res = A * B;        
	return Res;
}

f32 operator*(v3 A, v3 B)
{
	f32 Res = A.X * B.X + A.Y * B.Y + A.Z * B.Z;
	return Res;
}

////////////////////////////////////////////////////////////////////////////////
// V4 Operations

b32 operator==(v4 A, v4 B)
{
	return (A.X == B.X) && (A.Y == B.Y) && (A.Z == B.Z) && (A.W == B.W);
}

b32 operator!=(v4 A, v4 B)
{
	return (A.X != B.X) || (A.Y != B.Y) || (A.Z != B.Z) || (A.W != B.W);
}

v4 operator+(v4 A, v4 B)
{
	v4 Res = A;
	Res.X += B.X;
	Res.Y += B.Y;
	Res.Z += B.Z;
	Res.W += B.W;
	return Res;
}

v4 operator-(v4 A)
{
	v4 Res = V(-A.X, -A.Y, -A.Z, -A.W);
	return Res;
}

v4 operator-(v4 A, v4 B)
{
	v4 Res = A + (-B);
	return Res;
}

v4 operator*(v4 A, f32 B)
{
	v4 Res = A;
	Res.X *= B;
	Res.Y *= B;
	Res.Z *= B;
	Res.W *= B;
	return Res;
}

v4 operator*(f32 B, v4 A)
{
	v4 Res = A * B;        
	return Res;
}

f32 operator*(v4 A, v4 B)
{
	f32 Res = A.X * B.X + A.Y * B.Y + A.Z * B.Z + A.W * B.W;
	return Res;
}

////////////////////////////////////////////////////////////////////////////////
// IV2 Operations

b32 operator==(iv2 A, iv2 B)
{
	return (A.X == B.X) && (A.Y == B.Y);
}

b32 operator!=(iv2 A, iv2 B)
{
	return (A.X != B.X) || (A.Y != B.Y);
}

iv2 operator+(iv2 A, iv2 B)
{
	iv2 Res = A;
	Res.X += B.X;
	Res.Y += B.Y;
	return Res;
}

iv2 operator-(iv2 A)
{
	iv2 Res = IV(-A.X, -A.Y);
	return Res;
}

iv2 operator-(iv2 A, iv2 B)
{
	iv2 Res = A + (-B);
	return Res;
}

iv2 operator*(iv2 A, i32 B)
{
	iv2 Res = A;
	Res.X *= B;
	Res.Y *= B;
	return Res;
}

iv2 operator*(i32 B, iv2 A)
{
	iv2 Res = A * B;
	return Res;
}

i32 operator*(iv2 A, iv2 B)
{
	i32 Res = A.X * B.X + A.Y * B.Y;
	return Res;
}

////////////////////////////////////////////////////////////////////////////////
// IV3 Operations

b32 operator==(iv3 A, iv3 B)
{
	return (A.X == B.X) && (A.Y == B.Y) && (A.Z == B.Z);
}

b32 operator!=(iv3 A, iv3 B)
{
	return (A.X != B.X) || (A.Y != B.Y) || (A.Z != B.Z);
}

iv3 operator+(iv3 A, iv3 B)
{
	iv3 Res = A;
	Res.X += B.X;
	Res.Y += B.Y;
	Res.Z += B.Z;
	return Res;
}

iv3 operator-(iv3 A)
{
	iv3 Res = IV(-A.X, -A.Y, -A.Z);
	return Res;
}

iv3 operator-(iv3 A, iv3 B)
{
	iv3 Res = A + (-B);
	return Res;
}

iv3 operator*(iv3 A, i32 B)
{
	iv3 Res = A;
	Res.X *= B;
	Res.Y *= B;
	Res.Z *= B;
	return Res;
}

iv3 operator*(i32 B, iv3 A)
{
	iv3 Res = A * B;        
	return Res;
}

i32 operator*(iv3 A, iv3 B)
{
	i32 Res = A.X * B.X + A.Y * B.Y + A.Z * B.Z;
	return Res;
}

////////////////////////////////////////////////////////////////////////////////
// IV4 Operations

b32 operator==(iv4 A, iv4 B)
{
	return (A.X == B.X) && (A.Y == B.Y) && (A.Z == B.Z) && (A.W == B.W);
}

b32 operator!=(iv4 A, iv4 B)
{
	return (A.X != B.X) || (A.Y != B.Y) || (A.Z != B.Z) || (A.W != B.W);
}

iv4 operator+(iv4 A, iv4 B)
{
	iv4 Res = A;
	Res.X += B.X;
	Res.Y += B.Y;
	Res.Z += B.Z;
	Res.W += B.W;
	return Res;
}

iv4 operator-(iv4 A)
{
	iv4 Res = IV(-A.X, -A.Y, -A.Z, -A.W);
	return Res;
}

iv4 operator-(iv4 A, iv4 B)
{
	iv4 Res = A + (-B);
	return Res;
}

iv4 operator*(iv4 A, i32 B)
{
	iv4 Res = A;
	Res.X *= B;
	Res.Y *= B;
	Res.Z *= B;
	Res.W *= B;
	return Res;
}

iv4 operator*(i32 B, iv4 A)
{
	iv4 Res = A * B;        
	return Res;
}

i32 operator*(iv4 A, iv4 B)
{
	i32 Res = A.X * B.X + A.Y * B.Y + A.Z * B.Z + A.W * B.W;
	return Res;
}

////////////////////////////////////////////////////////////////////////////////
// UV2 Operations

b32 operator==(uv2 A, uv2 B)
{
	return (A.X == B.X) && (A.Y == B.Y);
}

b32 operator!=(uv2 A, uv2 B)
{
	return (A.X != B.X) || (A.Y != B.Y);
}

uv2 operator+(uv2 A, uv2 B)
{
	uv2 Res = A;
	Res.X += B.X;
	Res.Y += B.Y;
	return Res;
}

uv2 operator-(uv2 A)
{
	uv2 Res = UV(-A.X, -A.Y);
	return Res;
}

uv2 operator-(uv2 A, uv2 B)
{
	uv2 Res = A + (-B);
	return Res;
}

uv2 operator*(uv2 A, u32 B)
{
	uv2 Res = A;
	Res.X *= B;
	Res.Y *= B;
	return Res;
}

uv2 operator*(u32 B, uv2 A)
{
	uv2 Res = A * B;
	return Res;
}

u32 operator*(uv2 A, uv2 B)
{
	u32 Res = A.X * B.X + A.Y * B.Y;
	return Res;
}

////////////////////////////////////////////////////////////////////////////////
// UV3 Operations

b32 operator==(uv3 A, uv3 B)
{
	return (A.X == B.X) && (A.Y == B.Y) && (A.Z == B.Z);
}

b32 operator!=(uv3 A, uv3 B)
{
	return (A.X != B.X) || (A.Y != B.Y) || (A.Z != B.Z);
}

uv3 operator+(uv3 A, uv3 B)
{
	uv3 Res = A;
	Res.X += B.X;
	Res.Y += B.Y;
	Res.Z += B.Z;
	return Res;
}

uv3 operator-(uv3 A)
{
	uv3 Res = UV(-A.X, -A.Y, -A.Z);
	return Res;
}

uv3 operator-(uv3 A, uv3 B)
{
	uv3 Res = A + (-B);
	return Res;
}

uv3 operator*(uv3 A, u32 B)
{
	uv3 Res = A;
	Res.X *= B;
	Res.Y *= B;
	Res.Z *= B;
	return Res;
}

uv3 operator*(u32 B, uv3 A)
{
	uv3 Res = A * B;        
	return Res;
}

u32 operator*(uv3 A, uv3 B)
{
	u32 Res = A.X * B.X + A.Y * B.Y + A.Z * B.Z;
	return Res;
}

////////////////////////////////////////////////////////////////////////////////
// UV4 Operations

b32 operator==(uv4 A, uv4 B)
{
	return (A.X == B.X) && (A.Y == B.Y) && (A.Z == B.Z) && (A.W == B.W);
}

b32 operator!=(uv4 A, uv4 B)
{
	return (A.X != B.X) || (A.Y != B.Y) || (A.Z != B.Z) || (A.W != B.W);
}

uv4 operator+(uv4 A, uv4 B)
{
	uv4 Res = A;
	Res.X += B.X;
	Res.Y += B.Y;
	Res.Z += B.Z;
	Res.W += B.W;
	return Res;
}

uv4 operator-(uv4 A)
{
	uv4 Res = UV(-A.X, -A.Y, -A.Z, -A.W);
	return Res;
}

uv4 operator-(uv4 A, uv4 B)
{
	uv4 Res = A + (-B);
	return Res;
}

uv4 operator*(uv4 A, u32 B)
{
	uv4 Res = A;
	Res.X *= B;
	Res.Y *= B;
	Res.Z *= B;
	Res.W *= B;
	return Res;
}

uv4 operator*(u32 B, uv4 A)
{
	uv4 Res = A * B;        
	return Res;
}

u32 operator*(uv4 A, uv4 B)
{
	u32 Res = A.X * B.X + A.Y * B.Y + A.Z * B.Z + A.W * B.W;
	return Res;
}


////////////////////////////////////////////////////////////////////////////////
// RECT Operactions

rect operator+(rect Rect, v2 Offset)
{
        rect Result;
	Result.TopRight = Rect.TopRight + Offset;
	Result.BottomLeft = Rect.BottomLeft + Offset;
	return Result;
}
rect operator-(rect Rect, v2 Offset)
{
        rect Result;
	Result.TopRight = Rect.TopRight - Offset;
	Result.BottomLeft = Rect.BottomLeft - Offset;
	return Result;
}
rect operator*(rect Rect, f32 factor)
{
        rect Result;
	Result.TopRight = Rect.TopRight * factor;
	Result.BottomLeft = Rect.BottomLeft * factor;
	return Result;
}
rect operator+(v2 Offset, rect Rect)
{
	return Rect + Offset;
}
rect operator-(v2 Offset, rect Rect)
{
	return Rect - Offset;
}
rect operator*(f32 factor, rect Rect)
{
	return Rect * factor;
}

////////////////////////////////////////////////////////////////////////////////
// IRECT Operactions

irect operator+(irect Rect, iv2 Offset)
{
        irect Result;
	Result.TopRight = Rect.TopRight + Offset;
	Result.BottomLeft = Rect.BottomLeft + Offset;
	return Result;
}
irect operator-(irect Rect, iv2 Offset)
{
        irect Result;
	Result.TopRight = Rect.TopRight - Offset;
	Result.BottomLeft = Rect.BottomLeft - Offset;
	return Result;
}
irect operator*(irect Rect, i32 factor)
{
        irect Result;
	Result.TopRight = Rect.TopRight * factor;
	Result.BottomLeft = Rect.BottomLeft * factor;
	return Result;
}
irect operator+(iv2 Offset, irect Rect)
{
	return Rect + Offset;
}
irect operator-(iv2 Offset, irect Rect)
{
	return Rect - Offset;
}
irect operator*(i32 factor, irect Rect)
{
	return Rect * factor;
}

