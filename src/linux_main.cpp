#ifdef __cplusplus
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <thread>
#include <math.h>
#else
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#endif


#include <def.h>

// for dllopen
#include <dlfcn.h>
// for checking wether the file changed
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

// #include <signal.h>
// #define MY_ASSERT(x) do {if (!(x)) raise(SIGTRAP);} while (0)

#include <nuklear.h>

#include <game.h>
#include <game_internal.h>

#include <opengl/init.h>
//#include <opengl/string.h>

#ifndef DLL_NAME
#define DLL_NAME "libyarpg.so"
#endif

char const Title[] = "yaRPG";

i32 width = 1000, height = 1000;
char const LibraryName[] = "libyarpg.so";
char LibraryLocation[2048];

// DURING_DEBUG(
// 	static i32 clamp(i32 input, i32 lower, i32 upper)
// 	{
// 		return input < lower ? lower :
// 			       input > upper ? upper :
// 			input;
// 	}
// 	);

// ASan flags every memory access outside of
// [0x000000000000, 0x00007fff7fff] (low)
// [0x10007fff8000, 0x7fffffffffff] (high)
// as an access violation
// so we have to choose a small BASE_ADDRESS
// On x64 the kernel is situated in the high
// addresses so this should not be a problem
void * const BASE_ADDRESS = (void *) (1 MB);

GLFWwindow *window;

void key_callback(GLFWwindow* window, int key, int scancode,
		  int action, int mods)
{

	if (action != GLFW_PRESS) return;
	// int32 new_x = cursor_x;
	// int32 new_y = cursor_y;
	// switch (key)
	// {
	// 	break;case GLFW_KEY_R: {
	// 		      // reload the dll

	// 	      }
	// 	break;case GLFW_KEY_A: rotation -= 0.05f;
	// 	break;case GLFW_KEY_E: rotation += 0.05f;
	// 	break;case GLFW_KEY_UP:    new_y += 1;
	// 	break;case GLFW_KEY_DOWN:  new_y -= 1;
	// 	break;case GLFW_KEY_LEFT:  new_x -= 1;
	// 	break;case GLFW_KEY_RIGHT: new_x += 1;
	// 	break;default: break;
	// }

	// cursor_x = clamp(new_x, -2, 2);
	// cursor_y = clamp(new_y, -2, 2);
}

struct game_code
{
	b32 IsValid;
	time_t Version;
	void *DynamicHandle;

	game_init *Init;
	game_update_and_render *UpdateAndRender;
	game_release *Release;
};

GAME_INIT(GameInitStub)
{
	return -1;
}

GAME_UPDATE_AND_RENDER(GameUpdateAndRenderStub)
{

}

GAME_RELEASE(GameReleaseStub)
{

}

game_code const INVALID_HANDLE = {
	.IsValid = false,
	.Version = 0,
	.DynamicHandle = NULL,
	.Init = GameInitStub,
	.UpdateAndRender = GameUpdateAndRenderStub,
	.Release = GameReleaseStub
};

#define CHECK_DL_ERROR() do { if((error_str = dlerror()) != NULL) \
		{puts(error_str); return GameHandle;}} while (0)

game_code LoadGameCode()
{
	char *error_str;

	game_code GameHandle = INVALID_HANDLE;
	struct stat FileStat;
	stat(LibraryLocation, &FileStat);
	GameHandle.Version = FileStat.st_mtime;
	GameHandle.DynamicHandle = dlopen(LibraryLocation, RTLD_NOW);

        CHECK_DL_ERROR();


	GameHandle.Init = (game_init *) dlsym(GameHandle.DynamicHandle, "GameInit");

	CHECK_DL_ERROR();

	GameHandle.UpdateAndRender = (game_update_and_render *)
		dlsym(GameHandle.DynamicHandle,
		      "GameUpdateAndRender");

	CHECK_DL_ERROR();

	GameHandle.Release = (game_release *) dlsym(GameHandle.DynamicHandle, "GameRelease");

	CHECK_DL_ERROR();

	GameHandle.IsValid = true;
	return GameHandle;
}

// TODO: use inotify instead
b32 ShouldReload(game_code *GameHandle)
{
	if (GameHandle->IsValid)
	{
		struct stat FileStat;
		stat(LibraryLocation, &FileStat);

		return FileStat.st_mtime != GameHandle->Version;
	} else {
                return true;
	}

}

void ReleaseGameCode(game_code GameHandle)
{
	if (GameHandle.DynamicHandle)
	{
		dlclose(GameHandle.DynamicHandle);
	}
}

enum record_state { IDLE, RECORDING, REPLAYING};

struct input_record
{
	record_state State;
	u64 Size;
	void *Memory;
	FILE *Handle;
};

void StartRecord(input_record *R)
{
	assert(R->State == IDLE);
	R->Handle = fopen("input_record", "wb");
	if (R->Handle == NULL)
	{
		return;
	}
	R->State = RECORDING;
	u64 BytesWritten = fwrite(R->Memory, 1, R->Size, R->Handle);
	assert(BytesWritten == R->Size);
}

void RecordInput(input_record *R, game_input I)
{
	assert(R->State == RECORDING);
	u64 BytesWritten = fwrite(&I, 1, sizeof(I), R->Handle);
	assert(BytesWritten == sizeof(I));
}

void StopRecord(input_record *R)
{
	assert(R->State == RECORDING);
	fclose(R->Handle);
	R->State = IDLE;
}

void StartReplay(input_record *R)
{
        assert(R->State == IDLE);

        R->Handle = fopen("input_record", "rb");
	if (R->Handle == NULL)
	{
		return;
	}
	R->State = REPLAYING;
	u64 BytesRead = fread(R->Memory, 1, R->Size, R->Handle);
	assert(BytesRead == R->Size);
}

void StopReplay(input_record *R)
{
	assert(R->State == REPLAYING);
	R->State = IDLE;
	fclose(R->Handle);
}

game_input ReplayRecord(input_record *R)
{
	assert(R->State == REPLAYING);
	game_input I;
	u64 BytesRead = fread(&I, 1, sizeof(I), R->Handle);
	if (BytesRead < sizeof(I))
	{
		assert(BytesRead == 0);
		StopReplay(R);
		StartReplay(R);
		I = ReplayRecord(R);
	}

	return I;
}

input_record InputRecord;
platform     Platform;
int main(int ArgumentCount, char *Arguments[])
{
	char *ExecutablePath = Arguments[0];
	printf("Executable Path: %s\n", ExecutablePath);
	// we want to get the directory that the programs runs in
	char *LastPathSeperator = ExecutablePath;
	char *ProgramDirectory  = ExecutablePath;
	while (*ExecutablePath)
	{
		if (*ExecutablePath == '/')
			LastPathSeperator = ExecutablePath;
		ExecutablePath += 1;
	}
	assert(LastPathSeperator != ExecutablePath);
	LastPathSeperator[1] = '\0';

	// TODO: make this more dynamic, also remove global by
	// passing a string to Load-/ShouldReloadCode instead
	assert(snprintf(LibraryLocation, sizeof(LibraryLocation),"%s%s",
			ProgramDirectory,
			LibraryName) <= (i32)sizeof(LibraryLocation));


	opengl options = {
		.major = 3, .minor = 3, .profile = GLFW_OPENGL_CORE_PROFILE,
		.forward_compat = GL_TRUE, .resizable = GL_FALSE,
		.fullscreen = GL_FALSE, .anti_alias = 0
	};
	window = init(options, Title, width, height);

        printf("OpenGL version used: %s\n", glGetString(GL_VERSION));

	glfwSetKeyCallback(window, key_callback);
	errno = 0;
	game_memory Memory = {
		.PersistentSize = 20 MB,
		.TransientSize = 20 MB,
		.PersistentMemory = NULL,
		.TransientMemory = NULL};

	printf("%lu\n", Memory.PersistentSize);
	InitNuklear(&Platform.Ctx, 6 MB,
		    calloc(1, 6 MB));

	// NOTE: we use mmap here to always have the same base address
	// this allows us to backup and reload game data very easily
        Memory.PersistentMemory = mmap(
		BASE_ADDRESS, Memory.PersistentSize + Memory.TransientSize,
		// MAP_FIXED -> force it to start at BASE_ADDRESS
		// MAP_ANONYMOUS -> dont load a file, just write 0s
		// maybe we should request huge pages ?
		PROT_READ|PROT_WRITE, MAP_FIXED|MAP_ANONYMOUS|MAP_SHARED,
		// as described in:
		// https://man7.org/linux/man-pages/man2/mmap.2.html
		// we should use -1 and 0 here
		// since we arent backed by a file
		-1, 0);

	if (Memory.PersistentMemory != BASE_ADDRESS) perror("mmap error");

	assert(Memory.PersistentMemory != MAP_FAILED);
	assert(Memory.PersistentMemory == BASE_ADDRESS);
//	Memory.TransientMemory  = malloc(Memory.TransientSize);
	Memory.TransientMemory = PTROFFSET(Memory.PersistentMemory, Memory.PersistentSize);

	game_code GameHandle = LoadGameCode();

        assert(GameHandle.IsValid);
	assert(!GameHandle.Init(&Memory));


	InputRecord.Size = Memory.PersistentSize;
	InputRecord.Memory = Memory.PersistentMemory;

        game_input Input;
	i32 FrameCounter = 0;
//        string_state PlatformString;
//	string_setup(&PlatformString);
	float Red[3] = {1.0f, 0.0f, 0.0f};
	float Green[3] = {0.0f, 1.0f, 0.0f};
	float White[3] = {1.0f, 1.0f, 1.0f};
	while(!glfwWindowShouldClose(window))
	{
                if (ShouldReload(&GameHandle))
		{
			ReleaseGameCode(GameHandle);
			GameHandle = LoadGameCode();
			if (GameHandle.IsValid)
			{
				puts("Loaded new version.");
			}
			else
			{
				puts("Couldnt load new version.");
			}

		}
                glClearColor(0.0f, 0.0f, 0.0f, 1.0f );
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		Input.State[KEY_W] = (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS);
		Input.State[KEY_A] = (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS);
		Input.State[KEY_S] = (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS);
		Input.State[KEY_D] = (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS);
                double WeirdX, WeirdY;
		glfwGetCursorPos(window, &WeirdX, &WeirdY);
		// TODO: should use floor instead!
		Input.MouseX = (i32) WeirdX;
		Input.MouseY = (i32) WeirdY;
		Input.MouseDown = (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1)
				   == GLFW_PRESS);

		ParseInputsNuklear(&Platform.Ctx, &Input);

		if (InputRecord.State == RECORDING)
		{
			RecordInput(&InputRecord, Input);
		} else if (InputRecord.State == REPLAYING) {
			Input = ReplayRecord(&InputRecord);
		}


		GameHandle.UpdateAndRender(&Platform, &Memory, Input);

		game *GameState = (game *) Memory.PersistentMemory;

		if (nk_begin(&Platform.Ctx.Nuklear, "PlatformDebug", nk_rect(50, 50, 200, 200),
			     NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_CLOSABLE)) {
			/* fixed widget pixel width */

			nk_layout_row_static(&Platform.Ctx.Nuklear, 30, 50, 3);
			switch (InputRecord.State)
			{
				break;case IDLE:
                                
				if (nk_button_label(&Platform.Ctx.Nuklear, "Record")) {
					StartRecord(&InputRecord); 
				}
				nk_label(&Platform.Ctx.Nuklear, "Stop", NK_TEXT_CENTERED);
				if (nk_button_label(&Platform.Ctx.Nuklear, "Replay")) {
					StartReplay(&InputRecord);
				}
				break;case RECORDING:
				nk_label(&Platform.Ctx.Nuklear, "Record", NK_TEXT_CENTERED);
				if (nk_button_label(&Platform.Ctx.Nuklear, "Stop"))
				{
					StopRecord(&InputRecord);
				}
				if (nk_button_label(&Platform.Ctx.Nuklear, "Replay"))
				{
					StopRecord(&InputRecord);
					StartReplay(&InputRecord);
				}                                
				break;case REPLAYING:
                                if (nk_button_label(&Platform.Ctx.Nuklear, "Record"))
				{
					StopReplay(&InputRecord);
					StartRecord(&InputRecord);
				}
				if (nk_button_label(&Platform.Ctx.Nuklear, "Stop"))
				{
					StopReplay(&InputRecord);
				}
				nk_label(&Platform.Ctx.Nuklear, "Replay", NK_TEXT_CENTERED);
			}
                        
			nk_layout_row_begin(&Platform.Ctx.Nuklear, NK_STATIC, 30, 2);
                        nk_layout_row_push(&Platform.Ctx.Nuklear, 100);
			nk_label(&Platform.Ctx.Nuklear, "Currently:", NK_TEXT_LEFT);
			nk_layout_row_push(&Platform.Ctx.Nuklear, 50);
			switch (InputRecord.State)
			{
				break;case RECORDING:
				nk_label(&Platform.Ctx.Nuklear, "Recording", NK_TEXT_RIGHT);

				break;case IDLE:
				nk_label(&Platform.Ctx.Nuklear, "Idle", NK_TEXT_RIGHT);

				break;case REPLAYING:
				nk_label(&Platform.Ctx.Nuklear, "Replaying", NK_TEXT_RIGHT);
			}

			char buffer[64];

			snprintf(buffer, sizeof(buffer), "%d", GameState->World.NumActiveEntities);
			
			nk_layout_row_begin(&Platform.Ctx.Nuklear, NK_STATIC, 30, 2);
                        nk_layout_row_push(&Platform.Ctx.Nuklear, 100);
			nk_label(&Platform.Ctx.Nuklear, "NumActiveEntities", NK_TEXT_LEFT);
			nk_layout_row_push(&Platform.Ctx.Nuklear, 50);
			nk_label(&Platform.Ctx.Nuklear, buffer, NK_TEXT_RIGHT);

			snprintf(buffer, sizeof(buffer), "%d", GameState->World.NumLoadedEntities);
			
			nk_layout_row_begin(&Platform.Ctx.Nuklear, NK_STATIC, 30, 2);
                        nk_layout_row_push(&Platform.Ctx.Nuklear, 100);
			nk_label(&Platform.Ctx.Nuklear, "NumLoadedEntities", NK_TEXT_LEFT);
			nk_layout_row_push(&Platform.Ctx.Nuklear, 50);
			nk_label(&Platform.Ctx.Nuklear, buffer, NK_TEXT_RIGHT);
		}
		nk_end(&Platform.Ctx.Nuklear);








		struct nk_vec2 scale = {1.0, 1.0};
		DrawNuklear(&Platform.Ctx, width, height,
			    scale, NK_ANTI_ALIASING_ON);
		// switch(InputRecord.State)
		// {
		// 	break;case RECORDING:
		// 	{
		// 		print_string(&PlatformString,
		// 			     1.0f, 50.0f, 0.5f, Green,
		// 			     9, "Recording");
		// 	}
		// 	break;case IDLE:
		// 	{
		// 		print_string(&PlatformString,
		// 			     1.0f, 50.0f, 0.5f, White,
		// 			     4, "Idle");
		// 	}
		// 	break;case REPLAYING:
		// 	{
		// 		print_string(&PlatformString,
		// 			     1.0f, 50.0f, 0.5f, Red,
		// 			     9, "Replaying");
		// 	}

		// }


		glfwSwapBuffers(window);
		glfwPollEvents();




		FrameCounter++;
	}

	GameHandle.Release(&Memory);

	terminate(window);

	munmap(Memory.PersistentMemory, Memory.PersistentSize);
	free(Memory.TransientMemory);

	ReleaseGameCode(GameHandle);


}
