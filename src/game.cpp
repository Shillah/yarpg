// Platform independent game code.

#include <game.h>
#include <game_internal.h>

#include <math.h>

#include <opengl/shader.h>
#include <opengl/renderer.h>


#include <cstring>

#include <Nuklear/nuklear.h>

f32 window_vertices[] = {
	-1.0, -1.0,
	-1.0, 1.0,
	1.0, 1.0,
	1.0, -1.0
};

f32 window_colors[] = {
	1.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 1.0f,
	0.5f, 0.5f, 0.5f,
};

GLuint window_indices[] = {
        1, 0, 2, 3
};

GLuint indices[] = {
        0,1,5,2,4,3

};

GLuint cursor_indices[] = {
	0, 1, 2, 3, 4, 5, 0
};

enum color { GREEN, RED, BLUE, NUM_COLORS };

f32 color_value[][3] = {
	[GREEN] = {0.0, 1.0, 0.0},
	[RED]   = {1.0, 0.0, 0.0},
	[BLUE]  = {0.0, 0.0, 1.0}
};

enum {EASY, HARD};

static void MakeCamera(
	f32 CameraMatrix[4][4], // should be all 0
        v2  Middle, // in Meter, z assumed to be 0
	v3  Radius // in Meter, xrad, yrad, zrad
	)
{
	for (i32 Row = 0; Row < 4; ++Row)
	{
		for (i32 Column = 0; Column < 4; ++Column)
		{
			CameraMatrix[Row][Column] = 0.0f;
		}
		CameraMatrix[Row][Row] = 1.0f;
	}

	CameraMatrix[0][0] = 1/Radius.X; // = 2/(right-left)
	CameraMatrix[1][1] = 1/Radius.Y;
	CameraMatrix[2][2] = -1/Radius.Z;
	CameraMatrix[3][3] = 1;
	CameraMatrix[0][3] = -Middle.X/Radius.X;
	CameraMatrix[1][3] = -Middle.Y/Radius.Y;
	CameraMatrix[2][3] = 0;
}

static gpu_mesh load_mesh_with(program *Shader, GLuint VAO, GLuint VBO[3],
			       GLenum DrawMode, mesh Mesh)
{
	glBindVertexArray(VAO);

        ///////////////////////////////////////////////////////////////////////
	/// LOAD POSITION DATA
	glBindBuffer(GL_ARRAY_BUFFER, VBO[VERTEX_DATA]);
	glBufferData(GL_ARRAY_BUFFER,
		     sizeof(typeof(*Mesh.vertices)) * 2 * Mesh.num_vertices,
	 	     Mesh.vertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(Shader->Position);
	glVertexAttribPointer(Shader->Position, 2, GL_FLOAT, GL_FALSE, 0, 0);

	///////////////////////////////////////////////////////////////////////
	/// LOAD COLOR DATA

	if (Mesh.vertex_colors)
	{
		glBindBuffer(GL_ARRAY_BUFFER, VBO[COLOR_DATA]);
		glBufferData(GL_ARRAY_BUFFER,
			     sizeof(typeof(*Mesh.vertex_colors)) * 3 * Mesh.num_vertices,
			     Mesh.vertex_colors, GL_STATIC_DRAW);
		if (Shader->Color >= 0)
		{
			glEnableVertexAttribArray(Shader->Color);
			glVertexAttribPointer(Shader->Color, 3, GL_FLOAT, GL_FALSE, 0, 0);
		}
	}


	////////////////////////////////////////////////////////////////////////
	/// LOAD INDEX DATA
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[INDEX_DATA]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		     sizeof(typeof(*Mesh.indices)) * Mesh.num_indices,
	 	     Mesh.indices, GL_STATIC_DRAW);

	////////////////////////////////////////////////////////////////////////
	/// CLEAN UP
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	gpu_mesh gm = {
		VAO, {VBO[VERTEX_DATA], VBO[COLOR_DATA], VBO[INDEX_DATA]},
		Mesh.num_indices, DrawMode
	};

	return gm;

}

static gpu_mesh load_mesh(program *shader, GLenum draw_mode, mesh m)
{
	GLuint vbo[3];
	glGenBuffers(3, vbo);

	GLuint vao;
	glGenVertexArrays(1, &vao);

	return load_mesh_with(shader, vao, vbo, draw_mode, m);
}

void release_mesh(gpu_mesh m)
{
	glDeleteVertexArrays(1, &m.vao);
	glDeleteBuffers(3, m.vbo);
}

EXTERN GAME_INIT(GameInit)
{
	char *Persistent = (char *) Memory->PersistentMemory;
	game *CurrentGame = (game *) Persistent;
	CurrentGame->ZoomFactor = 1.0f;
        printf("%lu\n", sizeof(struct game));
	Persistent += sizeof(struct game);

	InitializeArena(&CurrentGame->Arena, Persistent,
			Memory->PersistentSize - sizeof(struct game));
	InitializeArena(&CurrentGame->TransientArena, Memory->TransientMemory,
			Memory->TransientSize);

	CurrentGame->Value = 0.0f;
	CurrentGame->Difficulty = EASY;

	for (i32 y = 0; y < 4; y++)
	{
		for (i32 x = 0; x < 4; x++)
		{
			CurrentGame->CameraMatrix[y][x] = 0.0f;
		}
	}


	u32 BufferSize = 4096;
	char *VertexSource = PushArray(&CurrentGame->TransientArena, BufferSize, char);
	char *FragmentSource = PushArray(&CurrentGame->TransientArena, BufferSize, char);

	for (char *Cur = VertexSource; *Cur; ++Cur)
	{
                *Cur = '\0';
	}
	for (char *Cur = FragmentSource; *Cur; ++Cur)
	{
                *Cur = '\0';
	}

	// TODO: this should be relative to the executable root
	// or instead load from an asset instead
	FILE *f;
	f = open_file("res/shader/DefaultFragment.glsl");
	YARPG_ASSERT(f);
	read_file(f, 0, BufferSize, FragmentSource);
	close_file(f);
	f = open_file("res/shader/DefaultVertex.glsl");
	YARPG_ASSERT(f);
	read_file(f, 0, BufferSize, VertexSource);
	close_file(f);

	CurrentGame->DefaultProgram = make_program("DefaultProgram",
						  VertexSource,
						  NULL, FragmentSource);
        GL_QUICK_CHECK();
	RendererCreate(&CurrentGame->Renderer, &CurrentGame->DefaultProgram);
	GL_QUICK_CHECK();

	InitWorld(&CurrentGame->World, &CurrentGame->Arena);

	world_position Origin = {IV3(0, 0, 0), V2(0, 0)};
	f32 Color[3] = {1.0, 0.0, 1.0};
	AddCreature(&CurrentGame->World, Origin, Color);

	CurrentGame->ProjectionLocation =
		glGetUniformLocation(CurrentGame->DefaultProgram.Id, "Projection");

//	YARPG_ASSERT(CurrentGame->ProjectionLocation != -1);

	CurrentGame->StringState = PushStruct(&CurrentGame->Arena, string_state);
	string_setup(CurrentGame->StringState);

	InitGui(&CurrentGame->Gui, CurrentGame->StringState, &CurrentGame->DefaultProgram,
		1000, 1000);

	GL_QUICK_CHECK();
	for (char *Cur = VertexSource; *Cur; ++Cur)
	{
                *Cur = '\0';
	}
	for (char *Cur = FragmentSource; *Cur; ++Cur)
	{
                *Cur = '\0';
	}

	f = open_file("res/shader/RoundedRectFragment.glsl");
	YARPG_ASSERT(f);
	read_file(f, 0, BufferSize, FragmentSource);
	close_file(f);
	f = open_file("res/shader/RoundedRectVertex.glsl");
	YARPG_ASSERT(f);
	read_file(f, 0, BufferSize, VertexSource);
	close_file(f);

	CurrentGame->WindowProgram = make_program(
		"WindowProgram", VertexSource,
		NULL, FragmentSource);
        GL_QUICK_CHECK();

	mesh window = {
		sizeof(window_indices) / sizeof(GLuint),
		sizeof(window_vertices) / (2 * sizeof(f32)),
		window_vertices, window_colors, window_indices
	};

	CurrentGame->gwindow = load_mesh(&CurrentGame->WindowProgram,
					GL_TRIANGLE_STRIP,
					window);

        GL_QUICK_CHECK();

        GL_QUICK_CHECK();

	DURING_DEBUG(
		performance_res stat = performance_stat();
		CurrentGame->flast = performance_query();
		CurrentGame->precision = stat.precision;
		);

	return 0;
}


#ifdef DEBUG
static void PrintPerformance(
	string_state *state,
	f32 X, f32 Y, f32 Scale,
	u64 Precision,
	performance_result Start,
	performance_result End)
{
	char DebugCharBuffer[2048];

	long ns = End.real_time - Start.real_time;
	double us = ns / 1000.0;
	double ms = us / 1000.0;
	double s  = ms / 1000.0;

	long cycles = End.cycles - Start.cycles;

	snprintf(DebugCharBuffer, sizeof(DebugCharBuffer),
		 "DRAW PERFORMANCE: (%luns precision)\n%-15s %10.2lf%%\n"
		 "%-15s %10.2lf\n%-15s %10.2lf\n",
                 Precision,
		 "frame budget:", (ms / 16) * 100,
		 "mcycles/frame:", (double) cycles/1000000.0d,
		 "DPS:", 1/s);
	f32 Color[3] = {1.0f, 1.0f, 1.0f};
	print_string(state,
		     X, Y, Scale, Color,
		     sizeof(DebugCharBuffer), DebugCharBuffer);
}
#endif

EXTERN GAME_UPDATE_AND_RENDER(GameUpdateAndRender)
{
	game *CurrentGame = (game *)Memory->PersistentMemory;

	// tile_index Index;
        // Index.ChunkId = 1024;
	// Index.ChunkIndex = 15;

	// v3 x = V(1, 2.0, 3.0);
	// v4 y = V(1.0, 2.0, 3.0, 4.0);

	// before we draw, we should probably have a prepare stage:

	// for each entity, if its gonna get drawn, ensure that its
	// data is loaded (i.e. inside a buffer) -- if it isnt, just load it
	// otherwise unload its data if necessary (how do we find out ?)

	// we should probably draw in 4 steps (in no particular order):

	// 1) draw everything with texture, transform & vertex data
	// 2) draw everything with color, transform & vertex data
	// 3) draw everything with transform & text
	// 4) draw everything with a custom shader!

	// 1, 2 & 4 should be exclusive to one another, but every entity
	// should be able to have a seperate 3.

	// each step has its own shader
	// should each object have its own vao or should it be shared per
	// shader? (is that even possible?)
        GL_QUICK_CHECK();
	glClear(GL_COLOR_BUFFER_BIT);
	DURING_DEBUG(
                GL_QUICK_CHECK();
		performance_result start, end;
		);

	DURING_DEBUG(
		start = performance_query();
		);

	world *World = &CurrentGame->World;
	world_position NewCameraPos = World->CameraPos;

	if (Input.State[KEY_W])
	{
		NewCameraPos.Offset.Y += 0.15;
	} else if (Input.State[KEY_S]) {
		NewCameraPos.Offset.Y -= 0.15;
	}
	if (Input.State[KEY_D])
	{
		NewCameraPos.Offset.X += 0.15;
	} else if (Input.State[KEY_A]) {
		NewCameraPos.Offset.X -= 0.15;
	}

	CanonizePosition(&NewCameraPos);
	SetCameraPos(World, NewCameraPos);


	CurrentGame->LastInput = Input;
	v2 Middle = NewCameraPos.Offset;

	v3 Radius = {8.0f/CurrentGame->ZoomFactor, 8.0f/CurrentGame->ZoomFactor, 1.0f};
	MakeCamera(CurrentGame->CameraMatrix,
                   Middle, Radius);

        GL_QUICK_CHECK();
        RendererPrepare(&CurrentGame->Renderer, &CurrentGame->TransientArena);
	DrawWorld(World, &CurrentGame->Renderer);
	PrepareFrame(&CurrentGame->Gui, &CurrentGame->TransientArena,
		     Input.MouseX, Input.MouseY, Input.MouseDown);
        GL_QUICK_CHECK();



	v3 Middle3;
	Middle3.X = Middle.X;
	Middle3.Y = Middle.Y;
	Middle3.Z = 0.0f;
	vertex_data VertexData[3];
	VertexData[0].Position  = Middle3 + V(-0.5, -0.5, 0.0);
	VertexData[0].Color     = V(1.0, 0.0, 0.0, 1.0);
	VertexData[0].Transform = V(0.0, 0.0, 1.0);

	VertexData[1].Position  = Middle3 + V(0.0, 0.0, 0.0);
	VertexData[1].Color     = V(0.0, 1.0, 0.0, 1.0);
	VertexData[1].Transform = V(0.0, 0.0, 1.0);

	VertexData[2].Position  = Middle3 + V(0.5, -0.5, 0.0);
	VertexData[2].Color     = V(0.0, 0.0, 1.0, 1.0);
	VertexData[2].Transform = V(0.0, 0.0, 1.0);

	DrawTriangles(&CurrentGame->Renderer, 3, VertexData);
        GL_QUICK_CHECK();

	RendererDraw(&CurrentGame->Renderer,
		     CurrentGame->ProjectionLocation,
		     (f32 *)CurrentGame->CameraMatrix);


        // Slider(&CurrentGame->Gui,
	//        &CurrentGame->Value, 0.0, 40000.0,
	//        1, 300, 600, 100, 100);

	// Slider(&CurrentGame->Gui,
	//        &CurrentGame->ZoomFactor, 0.1, 2.0,
	//        2, 300, 500, 100, 100);

	// for (i32 k = 0; k < (int) CurrentGame->Value; ++k)
	// {
	// 	i32 x = k / 200;
	// 	i32 y = k % 200;
	// 	if (Button(&CurrentGame->Gui, 5+k, 5 * x, 5 * y, 5, 5))
	// 	{
        //                 printf("%d was clicked\n", k);
	// 	}
	// }

	BeginWindow(&CurrentGame->Gui, 1, "Title 1", 50, 650, 300, 300, 0);

	Slider(&CurrentGame->Gui, 1, 0, 20, 100, 100,
	       &CurrentGame->ZoomFactor, 0.1, 2.0);

	EndWindow(&CurrentGame->Gui);

	BeginWindow(&CurrentGame->Gui, 2, "Title 2", 650, 650, 300, 300, 0);

	f32 InvertedZoom = 2.0 - CurrentGame->ZoomFactor;
	if (Slider(&CurrentGame->Gui, 1, 0, 20, 100, 100,
		   &InvertedZoom, 0.0, 1.9))
	{
                CurrentGame->ZoomFactor = 2.0 - InvertedZoom;
	}

	EndWindow(&CurrentGame->Gui);

	FinishFrame(&CurrentGame->Gui);


        GL_QUICK_CHECK();
	FreeAll(&CurrentGame->TransientArena);
	DURING_DEBUG(
		end = performance_query();
		PrintPerformance(
			CurrentGame->StringState,
			0.0f, 900.0f, 0.8f,
			CurrentGame->precision,
			start, end);

		);
	DURING_DEBUG(
                CurrentGame->flast = start;
                GL_QUICK_CHECK();
		);




	// Integrate with platform layer?
	// if (nk_begin(&Platform->Ctx.Nuklear, "Show", nk_rect(50, 50, 220, 220),
	// 	     NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_CLOSABLE)) {
	// 	/* fixed widget pixel width */
	// 	nk_layout_row_static(&Platform->Ctx.Nuklear, 30, 80, 1);
	// 	if (nk_button_label(&Platform->Ctx.Nuklear, "button")) {
	// 		/* event handling */
	// 	}

	// 	/* fixed widget window ratio width */
	// 	nk_layout_row_dynamic(&Platform->Ctx.Nuklear, 30, 2);
	// 	if (nk_option_label(&Platform->Ctx.Nuklear, "easy",
	// 			    CurrentGame->Difficulty == EASY))
	// 	{
	// 		CurrentGame->Difficulty = EASY;
	// 	}
	// 	if (nk_option_label(&Platform->Ctx.Nuklear, "hard",
	// 			    CurrentGame->Difficulty == HARD))
	// 	{
	// 		CurrentGame->Difficulty = HARD;
	// 	}

	// 	/* custom widget pixel width */
	// 	nk_layout_row_begin(&Platform->Ctx.Nuklear, NK_STATIC, 30, 2);
	// 	{
	// 		nk_layout_row_push(&Platform->Ctx.Nuklear, 50);
	// 		nk_label(&Platform->Ctx.Nuklear, "Volume:", NK_TEXT_LEFT);
	// 		nk_layout_row_push(&Platform->Ctx.Nuklear, 110);
	// 		nk_slider_float(&Platform->Ctx.Nuklear, 0.0f, &CurrentGame->Value, 40000.0f, 100.0f);
	// 	}
	// 	nk_layout_row_end(&Platform->Ctx.Nuklear);

	// 	nk_layout_row_begin(&Platform->Ctx.Nuklear, NK_STATIC, 30, 2);
	// 	{
	// 		nk_layout_row_push(&Platform->Ctx.Nuklear, 50);
	// 		nk_label(&Platform->Ctx.Nuklear, "Zoom:", NK_TEXT_LEFT);
	// 		nk_layout_row_push(&Platform->Ctx.Nuklear, 110);
	// 		nk_slider_float(&Platform->Ctx.Nuklear, 0.01f, &CurrentGame->ZoomFactor,
	// 				1.0f, 0.01f);
	// 	}
	// 	nk_layout_row_end(&Platform->Ctx.Nuklear);
	// }
	// nk_end(&Platform->Ctx.Nuklear);
}

EXTERN GAME_RELEASE(GameRelease)
{
	// this should probably all be done by the operating system instead.
	// maybe replace with dummy function in release mode?
	game *CurrentGame = (game *)Memory->PersistentMemory;
	release_mesh(CurrentGame->gm);
	release_mesh(CurrentGame->gwindow);
	release_mesh(CurrentGame->gcursor);
	string_release(CurrentGame->StringState);
}
