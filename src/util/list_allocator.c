#include <util/list_allocator.h>

#include <assert.h>
#include <string.h>
#include <stdio.h>

#define USEFUL_SIZE 10 KB
#define LIST_ALLOCATOR_BLOCK_ALIGNMENT 1 KB

static void PrintList(list_allocator *Alloc)
{
	memory_index CompleteSize = 0;
	for (memory_block *Current = Alloc->Sentinel.Next;
	     Current != &Alloc->Sentinel;
	     Current = Current->Next)
	{
		CompleteSize += Current->Size + sizeof(memory_block);
		printf("%s %lu ", (Current->Flags & ListAllocator_USED) ?
		       "USED" : "FREE", Current->Size);
	}
        printf("= %lu\n", CompleteSize);
}

static void AssertZeroedMemory(memory_index Size, void *Memory)
{
	byte *Mem = Memory;
	for (memory_index i = 0;
	     i < Size;
	     ++i)
	{
		assert(Mem[i] == 0);
	}
}

static void TryReclaimingMemory(list_allocator *Alloc)
{

}

static void InsertBlock(memory_block *Prev, memory_index Size, void *Memory)
{
	assert(sizeof(memory_block) < Size);
	AssertZeroedMemory(Size, Memory);
	memory_block *NewBlock = (memory_block *) Memory;
	NewBlock->Flags = 0;
	NewBlock->Prev = Prev;
	NewBlock->Next = Prev->Next;
	NewBlock->Prev->Next = NewBlock;
	NewBlock->Next->Prev = NewBlock;
	NewBlock->Size = Size - sizeof(*NewBlock);
}

b32  InitializeListAllocator(list_allocator *Alloc,
			     memory_index   MaxSize,
			     void           *Memory)
{
	void *AlignedMemory = NEXTALIGNED(Memory, LIST_ALLOCATOR_BLOCK_ALIGNMENT);
	memory_index Useless = (memory_index) AlignedMemory - (memory_index) Memory;
	Alloc->Sentinel.Prev  = &Alloc->Sentinel;
	Alloc->Sentinel.Next  = &Alloc->Sentinel;
	Alloc->Sentinel.Size  = Useless;
	Alloc->Sentinel.Flags = ListAllocator_USED | ListAllocator_SENTINEL;
	InsertBlock(&Alloc->Sentinel, MaxSize - Useless, AlignedMemory);
//	PrintList(Alloc);
	return 0;
}

void *SlowAlloc(list_allocator *Alloc, memory_index Size)
{
	Size += sizeof(memory_block);
        for (memory_block *Current = Alloc->Sentinel.Next;
	     Current != &Alloc->Sentinel;
	     Current = Current->Next)
	{
		if (!(Current->Flags & ListAllocator_USED) &&
		    Size <= Current->Size)
		{
			Current->Flags |= ListAllocator_USED;
			void *FreeMemory = (void *) (Current+1);
			void *NextPossible = NEXTALIGNED(PTROFFSET(FreeMemory, Size),
							 LIST_ALLOCATOR_BLOCK_ALIGNMENT);
			memory_index Offset = (memory_index) NextPossible - (memory_index) Current;
			memory_index NewBlockSize = Current->Size - Offset;

			if (NewBlockSize > USEFUL_SIZE)
			{
				assert(Size <= Offset);
				Current->Size = Offset;
				InsertBlock(Current,
                                            NewBlockSize,
					    NextPossible);
			}

                        return FreeMemory;
		}
	}

        assert(0 && "Out of Memory");
}

static void AbsorbBlock(memory_block *Absorber, memory_block *Absorbee)
{
	Absorber->Size += Absorbee->Size + sizeof(memory_block);
	Absorber->Next = Absorbee->Next;
        Absorber->Next->Prev = Absorber;
}

void *SlowRealloc(list_allocator *Alloc, memory_index Size, void *Old)
{
	memory_block *Block = (memory_block *) Old - 1;
	memory_block *Next = Block->Next;
	memory_block *Prev = Block->Prev;
	memory_block *Start = Block, *End = Block;
	b32 NextFree = !!(Next->Flags & ListAllocator_USED);
	b32 PrevFree = !!(Prev->Flags & ListAllocator_USED);
        memory_index MaxPossibleSize = Block->Size;
	if (Size < MaxPossibleSize)
	{
		return Old;
	}
	if (NextFree)
	{
		MaxPossibleSize += sizeof(memory_block);
		MaxPossibleSize += Next->Size;
		End = Next;
		if (Size < MaxPossibleSize) goto ReallocNext;
	}
	/* if (PrevFree) */
	/* { */
	/* 	MaxPossibleSize += sizeof(memory_block); */
	/* 	MaxPossibleSize += Prev->Size; */
        /*         Start = Prev; */
	/* 	if (Size < MaxPossibleSize) goto Realloc; */
	/* } */

        // SLOW PATH
	void *NewMem = SlowAlloc(Alloc, Size);
	memcpy(NewMem, Old, Block->Size);
	SlowFree(Old);
	return NewMem;

ReallocNext:
	AbsorbBlock(Block, Next);
	return Old;

}

void *SlowArrayAlloc(list_allocator *Alloc, u32 Count, memory_index Size)
{
	return SlowAlloc(Alloc, Count * Size);
}

void SlowFree(void *ToFree)
{
	memory_block *Block = (memory_block *) ToFree - 1;
	Block->Flags &= ~ListAllocator_USED;
	if (!(Block->Next->Flags & ListAllocator_USED)) AbsorbBlock(Block, Block->Next);
	if (!(Block->Prev->Flags & ListAllocator_USED))
	{
		Block = Block->Prev;
		AbsorbBlock(Block, Block->Next);
	}
	DURING_DEBUG(
		memset(Block+1, 0, Block->Size);
		);
}
