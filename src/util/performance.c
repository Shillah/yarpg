#include <util/performance.h>

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <x86intrin.h>

performance_res performance_stat(void)
{
	struct timespec spec;
	clock_getres(CLOCK_MONOTONIC, &spec);
        performance_res ret = {.precision = spec.tv_nsec};
	return ret;
}

performance_result performance_query(void)
{
	struct timespec spec;

	clock_gettime(CLOCK_MONOTONIC, &spec);
	long unsigned clock = __rdtsc();
        performance_result ret = {.real_time = spec.tv_nsec, .cycles = clock};
	return ret;
}
