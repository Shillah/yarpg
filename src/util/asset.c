// An asset file has the following structure:

// HEADER
// probably some kind of versioning sheme?
// header_size: uint64
// num_entries: uint64
// entry_names: *uint64[num_entries] -- sorted*, offsets into name_data
// entry_sizes: uint64[num_entries]
// entry_offsets: uint64[num_entries]
// name_data: char[?]
// DATA
// entry1 entry2 ... entryn -- where n == num_entries

// the order of the entries in the DATA section need not
// be the order of the entry_names/entry_sizes/entry_offsets

// we sort the strings thusly:
// c <= s <=> len(c) <= len(s), c[i] <= s[i] for all 0 < i < len(c)
// this should allow us to use binary search to find the entry index


// IDEA: store offsets from the start of the HEADER instead of start of
// the DATA section. That way we dont need data_offset

#if defined(TEST) && !defined(DEBUG)
#define DEBUG
#endif

#include <def.h>
#include <asset.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include <util/file.h>

int compare_str(const char *str1, const char *str2)
{
	char c1, c2;
	int cmp;        
	do {
		c1 = *str1++;
		c2 = *str2++;
		cmp = (c2 < c1) - (c1 < c2);
		// if one but not both strings end here
		// then cmp != 0 will be true
		if (cmp != 0) return cmp;
	} while (c1 != '\0' && c2 != '\0');
	// since '\0' is the smallest character
	// and always at the end of a string
	// we know that cmp is set to the right value here
	// in fact: cmp is always 0 here! (so lets just return 0)
	return 0;
}

void combine_into_asset(
	char const *asset_name,
	u8 num_entries, char const *filenames[num_entries])
{
	int error;
	FILE *asset_file = fopen(asset_name, "wb");

	assert(NULL != asset_file);
	uint16 entry_names[num_entries];
	uint64 entry_sizes[num_entries];
	uint64 entry_offsets[num_entries];

	// size of the header_size and num_entries fields.
	uint32 header_size = sizeof(header);
	// add the size of the entry_names, -sizes & -offsets arrays.
	header_size += num_entries * (sizeof(uint16)
				      + sizeof(uint64)
				      + sizeof(uint64));
	error = fseek(asset_file, header_size, SEEK_SET);
	assert(0 == error);
	assert((uint64) ftell(asset_file) == header_size);

	char buffer[1024];
	uint64 bytes_read, bytes_written;
	uint64 name_offset = 0;
	//write names
	for (u8 i = 0; i < num_entries; ++i)
	{
		uint64 name_length = strlen(filenames[i]);
		//                                      dont forget the 0!
		//                                      vvvvvvvvvvvvvvv
                bytes_written = fwrite(filenames[i], 1, name_length + 1, asset_file);		
		assert(name_length + 1 == bytes_written);
		header_size += bytes_written;

                
		entry_names[i] = name_offset;
		name_offset += bytes_written;
                
	}

	//write data
	uint64 current_offset = 0;
	for (u8 i = 0; i < num_entries; ++i)
	{
		FILE *to_write = fopen(filenames[i], "r");
		assert(NULL != to_write);
#ifdef DEBUG
		uint64 pen = ftell(asset_file);
		printf("Writing asset %u at offset %lu\n", i, pen);
#endif
		entry_sizes[i] = 0;
                do {
			bytes_read = fread(buffer, 1, sizeof(buffer), to_write);
			bytes_written = fwrite(buffer, 1, bytes_read, asset_file);
			assert(bytes_read == bytes_written);
			entry_sizes[i] += bytes_written;
		} while (bytes_read != 0);
		entry_offsets[i] = current_offset;
		current_offset += entry_sizes[i];
		fclose(to_write);
	}
	error = fseek(asset_file, 0, SEEK_SET);
	assert(0 == error);

#define GUARDED_WRITE_VAL(x)                              \
	{                                                 \
	bytes_written = fwrite((char *) &x, 1, sizeof(x), \
			       asset_file);               \
	assert(sizeof(x) == bytes_written);               \
	}
#define GUARDED_WRITE_PTR(x)                             \
	{                                                \
	bytes_written = fwrite((char *) x, 1, sizeof(x), \
			       asset_file);              \
	assert(sizeof(x) == bytes_written);              \
	}


	GUARDED_WRITE_VAL(header_size);
	GUARDED_WRITE_VAL(num_entries);

	printf("HEADER SIZE %u\n NUM_ENTRIES %d\n NAME LENGTH %lu\n",
	       header_size, (int)num_entries, name_offset);

	// to write the other fields we first need to "sort" them.
	u8 start = 0, end = num_entries - 1;
	u8 entry_indices[num_entries];
	for (u8 i = 0; i < num_entries; ++i)
	{
		entry_indices[i] = i;
	}

	// TODO WHY DOESNT THIS WORK ?
//#define SWAP(x, y, T) do { x = x ^ y; y = x ^ y; x = y ^ x;} while (0)
#define SWAP(x, y, T) do { T tmp = x; x = y; y = tmp; } while (0)
#define ENTRY_SWAP(i, j) {                                              \
		SWAP(entry_indices[i], entry_indices[j], u8);	\
		SWAP(entry_names[i], entry_names[j], uint16);		\
		SWAP(entry_sizes[i], entry_sizes[j], uint64);		\
		SWAP(entry_offsets[i], entry_offsets[j], uint64);		\
		}

	if (num_entries > 1)
	{
        
		u8 stack[num_entries];
		int top = -1;
#define PUSH(x) stack[++top] = (x)
#define POP() stack[top--]
#define NOT_EMPTY() (top >= 0)
#define PIVOT(start, end) ((end) - (start))/2

		PUSH(start);
		PUSH(end);

		while (NOT_EMPTY())
		{
			end = POP();
			start = POP();                

			// do one quicksort step for array [start .. end]
			// underflows when start = 0, but thats ok!
			u8 i = start;                

			// we use end as the pivot
			char const *pivot = filenames[entry_indices[end]];
			// first partition
			for (int j = start; j < end; ++j)
			{
				if (compare_str(filenames[entry_indices[j]], pivot) < 0)
				{
					printf("%u->%u: %s\n",
					       end, entry_names[end],
					       filenames[entry_indices[end]]);
					printf("%u->%u: %s\n",
					       j, entry_names[j],
					       filenames[entry_indices[j]]);
					ENTRY_SWAP(i, j);
					++i;
				}
			}                
			ENTRY_SWAP(i, end);

			// then do the "recursive" step
			if (i - 1 > start)
			{
				PUSH(start);
				PUSH(i-1);
			}

			if (i + 1  < end)
			{
				PUSH(i+1);
				PUSH(end);
			}
		}
	}

#ifdef DEBUG
	for (u8 i = 0; i < num_entries; ++i)
	{
		printf("%d %lu %lu\n",
		       (int)entry_names[i],
		       entry_sizes[i],
		       entry_offsets[i]);
	}
#endif

	GUARDED_WRITE_PTR(entry_names);
	GUARDED_WRITE_PTR(entry_sizes);
	GUARDED_WRITE_PTR(entry_offsets);
	
        fclose(asset_file);
}

asset_handle *open_asset_file(char const *filename)
{
	FILE *f = open_file(filename);
#ifdef DEBUG
	if (NULL == f)
	{
		printf("Could not open: %s\n", filename);
		abort();
	}
#else
	return NULL;
#endif
	uint32 header_size;        
	read_file(f, 0, sizeof(header_size), (char *) &header_size);
	printf("HEADER_SIZE: %u\n", header_size);
        
        asset_handle *a = (asset_handle *) malloc(ASSET_HANDLE_SIZE
						  + header_size);

        read_file(f, 0, header_size, (char *) &a->h);
	u8 num_entries = a->h.num_entries;
        a->file_handle = f;
	a->entry_names = (uint16 *) (&a->h + 1);
	a->entry_sizes = (uint64 *) (a->entry_names + num_entries);
	a->entry_offsets = (uint64 *) (a->entry_sizes + num_entries);
	a->name_data     = (char *) (a->entry_offsets + num_entries);
                       
	
	return a;
}

void release_asset_handle(asset_handle *a)
{
	// TODO: do this
	fclose(a->file_handle);
	free(a);
}

// 0 = error, decrement in other functions
// returns the entry index
uint64 find_asset(asset_handle *a, char const *asset_name)
{
	int cmp;
	u8 start = 0, end = a->h.num_entries - 1;
	// start inclusive, end exclusiv!
	u8 current;
	do {                
                current = (end + start)/2;                
		cmp = compare_str(
			asset_name,
			a->name_data + a->entry_names[current]);

		if (cmp < 0)
		{
			end = current - 1;
		} else if (cmp > 0)
		{
			start = current + 1;
		}
		else
		{
			return current + 1;
		}                
	} while (start <= end);

	return 0;
}

uint64 asset_size(asset_handle *a, uint64 index)
{
	assert(index != 0);
	uint64 i = index - 1;
	return a->entry_sizes[i];
}

uint64 load_asset(asset_handle *a, uint64 index, uint64 size, char *buffer)
{
	assert(index != 0);
	uint64 i = index - 1;
	uint64 offset = a->h.header_size + a->entry_offsets[i];
#ifdef DEBUG
	printf("Loading asset (index: %lu) from offset %lu\n",
	       index, offset);
	int error =  fseek(a->file_handle, offset, SEEK_SET);
	assert(error == 0);
	assert(ftell(a->file_handle) == offset);
#else
	fseek(a->file_handle, offset, SEEK_SET);
#endif        
	return fread(buffer, 1, size, a->file_handle);
}

#ifdef ASSET_MAIN

uint32 endianess32(uint32 num)
{
        return ((num>>24)&0xff) | // move byte 3 to byte 0
		((num<<8)&0xff0000) | // move byte 1 to byte 2
		((num>>8)&0xff00) | // move byte 2 to byte 1
		((num<<24)&0xff000000); // byte 0 to byte 3
}

uint16 endianess16(uint16 num)
{
	return (num>>8) | (num<<8);
}

int main(int argc, char *argv[])
{
	/* // argv[0] is always the name of the program */
	/* assert(argc > 2); */
        
	/* combine_into_asset(argv[1], (uint64) argc-1, (char const **) &argv[2]); */

	char const *x[] =
	{
		   "/home/sura/Projekte/C++/yaRPG/res/shader/fstring.glsl",
		   "/home/sura/Projekte/C++/yaRPG/res/shader/fragment.glsl",
		   "/home/sura/Projekte/C++/yaRPG/res/shader/vstring.glsl"
	};

	combine_into_asset("my.asset", 3, x);
	asset_handle *a = open_asset_file("my.asset");

	printf("HEADER: %u NUM: %u\n", a->h.header_size, a->h.num_entries);        
	for(int8 i = 0; i < a->h.num_entries; ++i)
	{
		printf("ENTRY %u %u %u %s\n",
		       a->entry_names[i],
		       a->entry_sizes[i],
		       a->entry_offsets[i],
		       a->name_data + a->entry_names[i]);                		
	}
        
	assert(sizeof(x) / sizeof(*x) == a->h.num_entries);
	uint64 res, size;
	for (int8 i = 0; i < a->h.num_entries; ++i)
	{
		res = find_asset(a, x[i]);
		assert(0 != res);
		size = asset_size(a, res);
                
		char *buffer = calloc(size+1, 1);
		load_asset(a, res, size, buffer);
		puts("Loaded asset:");
		puts(x[i]);
		printf("%lu %lu\n", res, size);
		puts("----------------");
		puts(buffer);
		puts("----------------");
		puts("\n\n");
		free(buffer);
			
	}
	
	release_asset_handle(a);


	
	return 0;
}

#endif

#ifdef TEST
int buffer_eq(uint64 size, char *b1, char *b2)
{
        for (uint64 i = 0; i < size; ++i)
	{
		if (b1[i] != b2[i])
		{
			printf("BAD INDEX: %lu\n", i);
			return 0;
		}
	}
	return 1;
}

int main(int argc, char *argv[])
{
	// SETUP
	u8 const NUM_ASSETS = 50;
	uint32 const FILE_SIZE = 64;
	// NOTE: better to use mkstemp
	/* char const asset_template[] = "assetXXXXXX"; */
	/* char const result_template[] = "resultXXXXXX"; */
	// 0th file is the asset file
        char filenames[NUM_ASSETS + 1][L_tmpnam];
	char const *asset_pointers[NUM_ASSETS];
	for(i8 i = 0; i < NUM_ASSETS; ++i)
	{
		asset_pointers[i] = filenames[i+1];
	}
	char *garbage = malloc(FILE_SIZE * NUM_ASSETS);
	for (u64 i = 0; i < FILE_SIZE * NUM_ASSETS; ++i)
	{
		garbage[i] = (char) rand();
	}
	for (u64 i = 0; i < NUM_ASSETS; ++i)
	{
		garbage[i*FILE_SIZE + 0] = '_';
		garbage[i*FILE_SIZE + 1] = '_';
		garbage[i*FILE_SIZE + 2] = 'S';
		garbage[i*FILE_SIZE + 3] = 'T';
		garbage[i*FILE_SIZE + 4] = 'A';
		garbage[i*FILE_SIZE + 5] = 'R';
		garbage[i*FILE_SIZE + 6] = 'T';
		garbage[i*FILE_SIZE + 7] = '_';
		garbage[i*FILE_SIZE + 8] = '_';

		garbage[(i+1)*FILE_SIZE - 1] = '_';
		garbage[(i+1)*FILE_SIZE - 2] = '_';
		garbage[(i+1)*FILE_SIZE - 3] = 'D';
		garbage[(i+1)*FILE_SIZE - 4] = 'N';
		garbage[(i+1)*FILE_SIZE - 5] = 'E';
		garbage[(i+1)*FILE_SIZE - 6] = '_';
		garbage[(i+1)*FILE_SIZE - 7] = '_';
	}
	for (i8 i = 0; i <= NUM_ASSETS; ++i)
	{
		// give tmp name
                char *res = tmpnam(filenames[i]);
		puts(filenames[i]);
                
		assert(NULL != res);
		// dont write into the asset file
		if (0 != i)
		{
			strcpy(garbage + (i-1)*FILE_SIZE+9, filenames[i]);
			// open tmp file
			FILE *tmp = fopen(filenames[i], "wb");
			assert(NULL != tmp);
			// write garbage into the test files
			u64 bytes_written = fwrite(garbage + (i-1) * FILE_SIZE,
						      1, FILE_SIZE, tmp);
			assert(FILE_SIZE == bytes_written);
			
			fclose(tmp);
			
			// self check
			char tmp_content[FILE_SIZE];
			tmp = fopen(filenames[i], "r");
			assert(NULL != tmp);
			u64 bytes_read = fread(tmp_content, 1, FILE_SIZE, tmp);
			assert(FILE_SIZE == bytes_read);
			assert(buffer_eq(FILE_SIZE, tmp_content, garbage
					 + (i-1) * FILE_SIZE));
			fclose(tmp);
		}
	}

	//TEST START

	// combine into an asset
	combine_into_asset(filenames[0], NUM_ASSETS, asset_pointers);

	
	//open the asset file
	asset_handle *a = open_asset_file(filenames[0]);
	// TODO: assert the header_size
	assert(NULL != a);
	assert(NUM_ASSETS == a->h.num_entries);
	printf("HEADER: %u NUM: %u\n", a->h.header_size, a->h.num_entries);        
	for(i8 i = 0; i < a->h.num_entries; ++i)
	{
		assert(FILE_SIZE == a->entry_sizes[i]);
		assert(FILE_SIZE * (a->entry_offsets[i] / FILE_SIZE)
		       == a->entry_offsets[i]);
		printf("ENTRY %u %lu %lu\n",
		       a->entry_names[i],
		       a->entry_sizes[i],
		       a->entry_offsets[i]);                		
	}
                
	u64 res, size;
	for (i8 i = 0; i < a->h.num_entries; ++i)
	{
		printf("Start asset %u: %s\n--------\n", i+1, filenames[i+1]);
		res = find_asset(a, filenames[i+1]);
		assert(0 != res);
		printf("Found at index: %lu\n", res);
                
		size = asset_size(a, res);

		assert(FILE_SIZE == size);
                
                char buffer[FILE_SIZE];
		
		load_asset(a, res, FILE_SIZE, buffer);                
		puts("successfully loaded asset.");
		assert(FILE_SIZE * i == a->entry_offsets[res-1]);
		assert(buffer_eq(
			       strlen(filenames[i+1]),
			       filenames[i+1],
			       a->name_data + a->entry_names[res-1]));
                assert(buffer_eq(FILE_SIZE, buffer, garbage + i * FILE_SIZE));
		printf("CHECK COMPLETED: %u, %lu\n--------\n", i+1, res);
	}
	
	release_asset_handle(a);

	for (i8 i = 0; i <= NUM_ASSETS; ++i)
	{
		assert(!remove(filenames[i]));
	}        
	free(garbage);
	return 0;
}
#endif
