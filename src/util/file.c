#include <util/file.h>

FILE *open_file(char const *name)
{
	return fopen(name, "rb");
}

void close_file(FILE *f)
{
	fclose(f);
}

u64 read_file(FILE *f, u64 offset,
		 u64 num_to_read,
		 char *buffer)
{
	fseek(f, offset, SEEK_SET);
	return (u64) fread(buffer, sizeof(char), num_to_read, f);
}

u64 SizeOfFile(FILE *f)
{
	fseek(f, 0, SEEK_END);
        return (u64) ftell(f);
}
