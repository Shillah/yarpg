#include <util/arena_allocator.h>
#include <assert.h>
#ifdef DEBUG
#include <string.h>
#endif

void InitializeArena(memory_arena *Arena, void *Base, memory_index Size)
{
        Arena->Size = Size;
	Arena->Base = (byte *) Base;
	Arena->Used = 0;
}

void *PushSize(memory_arena *Arena, memory_index Size)
{
	void *Result = NULL;
	assert((Arena->Used + Size) < Arena->Size);
	if ((Arena->Used + Size) < Arena->Size)
	{
		Result = Arena->Base + Arena->Used;
		Arena->Used += Size;                
	}

	return Result;
        
}

void *PushSizeAligned(memory_arena *Arena, memory_index Size, u32 Alignment)
{
	void *Result = NULL;
	void *Current = Arena->Base + Arena->Used;

	void *NextPossible = NEXTALIGNED(Current, Alignment);

	memory_index Used = (memory_index) NextPossible - (memory_index) Arena->Base;
	
	assert(Used + Size < Arena->Size);
	
	if (Used + Size < Arena->Size)
	{
		Result = Arena->Base + Used;
		Arena->Used = Used + Size;
	}

	return Result;
}

void FreeAll(memory_arena *Arena)
{
	DURING_DEBUG( memset(Arena->Base, 0, Arena->Used) );
	Arena->Used = 0;
}
