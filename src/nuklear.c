#include <nuklear.h>

#ifndef FONT_NAME
#error "Define Font Name"
#endif

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_IMPLEMENTATION
#include <Nuklear/nuklear.h>
#include <stdlib.h>
#include <stdio.h>

#include <opengl/opengl.h>
#include <util/arena_allocator.h>
#include <util/list_allocator.h>

struct nk_glfw_vertex {
    float Position[2];
    float TexturePos[2];
    nk_byte Color[4];
};

#define MAX_MEMORY 512 MB
#define MAX_VERTEX_MEMORY 512 * 1024
#define MAX_ELEMENT_MEMORY 128 * 1024

#define GUESSED_NK_ALIGNMENT 8
#define GUESSED_NK_MASK      (GUESSED_NK_ALIGNMENT-1)

static void *NuklearPermAlloc(nk_handle Handle, void *Old, nk_size Size)
{
	struct list_allocator *ListAlloc = (struct list_allocator *) Handle.ptr;
	void *Result = NULL;
	if (Old != NULL)
	{
		Result = SlowRealloc(ListAlloc, Size, Old);
	} else {
		Result = SlowAlloc(ListAlloc, Size);
	}
	assert(!((memory_index)Result & GUESSED_NK_MASK));
        return Result;
}

static void NuklearPermFree(nk_handle Handle, void *Old)
{
	struct list_allocator *ListAlloc = (struct list_allocator *) Handle.ptr;
	// its legal in C to "free" NULL
	// so we have to check here
	if (Old) SlowFree(Old);
}

static void *NuklearTempAlloc(nk_handle Handle, void *Old, nk_size Size)
{
	memory_arena *Arena = Handle.ptr;
	void *Result = NULL;
	Result = PushSizeAligned(Arena, Size, GUESSED_NK_ALIGNMENT);
	assert(!((memory_index)Result & GUESSED_NK_MASK));
	return Result;
}

static void NuklearTempFree(nk_handle Handle, void *Old)
{
	memory_arena *Arena = Handle.ptr;
	// Arenas dont get freed.
}

static void InitDevice(device *Graphics, struct nk_allocator *Allocator)
{
	GLint status;

	char const VertexSource[] =
		"#version 150\n"
		"\n"
		"uniform mat4 Projection;\n"
		"\n"
		"in vec2 Position;\n"
		"in vec2 TexturePos;\n"
		"in vec4 Color;\n"
		"\n"
		"out vec2 VertexTexturePos;\n"
		"out vec4 VertexColor;\n"
		"\n"
		"void main() {\n"
		"  VertexTexturePos = TexturePos;\n"
		"  VertexColor = Color;\n"
		"  gl_Position = Projection * vec4(Position.xy, 0, 1);\n"
		"}\n";
	char const FragmentSource[] =
		"#version 150\n"
		"\n"
		"precision mediump float;\n"
		"\n"
		"uniform sampler2D Texture;\n"
		"\n"
		"in vec2 VertexTexturePos;\n"
		"in vec4 VertexColor;\n"
		"\n"
		"out vec4 FragmentColor;\n"
		"\n"
		"void main(){\n"
		"	FragmentColor = VertexColor * texture(Texture, VertexTexturePos);\n"
		"}\n";

	nk_buffer_init(&Graphics->Commands, Allocator, NK_BUFFER_DEFAULT_INITIAL_SIZE);
	Graphics->Shader = make_program("Nuklear", VertexSource, NULL, FragmentSource);

	/* buffer setup */
	GLsizei VertexSize = sizeof(struct nk_glfw_vertex);
	size_t PositionOffset = offsetof(struct nk_glfw_vertex, Position);
	size_t TexPosOffset = offsetof(struct nk_glfw_vertex, TexturePos);
	size_t ColorOffset = offsetof(struct nk_glfw_vertex, Color);

	glGenBuffers(1, &Graphics->VertexBuffer);
	glGenBuffers(1, &Graphics->IndexBuffer);
	glGenVertexArrays(1, &Graphics->ArrayBuffer);

	glBindVertexArray(Graphics->ArrayBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, Graphics->VertexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Graphics->IndexBuffer);

	glBufferData(GL_ARRAY_BUFFER, MAX_VERTEX_MEMORY, NULL, GL_STREAM_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, MAX_ELEMENT_MEMORY, NULL, GL_STREAM_DRAW);

	glEnableVertexAttribArray((GLuint)Graphics->Shader.Position);
	glEnableVertexAttribArray((GLuint)Graphics->Shader.TexturePos);
	glEnableVertexAttribArray((GLuint)Graphics->Shader.Color);

	glVertexAttribPointer((GLuint)Graphics->Shader.Position, 2,
			      GL_FLOAT, GL_FALSE, VertexSize,
			      (void*)PositionOffset);
	glVertexAttribPointer((GLuint)Graphics->Shader.TexturePos, 2,
			      GL_FLOAT, GL_FALSE, VertexSize,
			      (void*)TexPosOffset);
	glVertexAttribPointer((GLuint)Graphics->Shader.Color, 4,
			      GL_UNSIGNED_BYTE, GL_TRUE, VertexSize,
			      (void*)ColorOffset);

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

static void InitFont(struct nk_font     **Font,
		     device              *Graphics,
		     struct nk_allocator *PermAlloc,
		     struct nk_allocator *TempAlloc)
{
	struct nk_font_atlas atlas;
	void const *Image;
	int ImageWidth, ImageHeight;
	struct nk_font_config Config = nk_font_config(0);
	Config.oversample_h = 3; Config.oversample_v = 2;
	nk_font_atlas_init_custom(&atlas, PermAlloc, TempAlloc);
	nk_font_atlas_begin(&atlas);
	*Font = nk_font_atlas_add_from_file(&atlas,
					       FONT_NAME,
					       14.0f,
					       &Config);

	Image = nk_font_atlas_bake(&atlas, &ImageWidth, &ImageHeight, NK_FONT_ATLAS_RGBA32);
                
	glGenTextures(1, &Graphics->FontTexture);
	GL_QUICK_CHECK();
	glBindTexture(GL_TEXTURE_2D, Graphics->FontTexture);
	GL_QUICK_CHECK();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	GL_QUICK_CHECK();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	GL_QUICK_CHECK();        
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)ImageWidth, (GLsizei)ImageHeight, 0,
		     GL_RGBA, GL_UNSIGNED_BYTE, Image);
	GL_QUICK_CHECK();        

	nk_font_atlas_end(&atlas, nk_handle_id((int) Graphics->FontTexture),
			  &Graphics->Nothing);


}

void ParseInputsNuklear(nuklear_context *Ctx, game_input *Input)
{
	nk_input_begin(&Ctx->Nuklear);
	/* nk_input_key(&Ctx->Nuklear, NK_KEY_DEL, glfwGetKey(win, GLFW_KEY_DELETE) == GLFW_PRESS); */
	/* nk_input_key(&Ctx->Nuklear, NK_KEY_ENTER, glfwGetKey(win, GLFW_KEY_ENTER) == GLFW_PRESS); */
	/* nk_input_key(&Ctx->Nuklear, NK_KEY_TAB, glfwGetKey(win, GLFW_KEY_TAB) == GLFW_PRESS); */
	/* nk_input_key(&Ctx->Nuklear, NK_KEY_BACKSPACE, glfwGetKey(win, GLFW_KEY_BACKSPACE) == GLFW_PRESS); */
	/* nk_input_key(&Ctx->Nuklear, NK_KEY_LEFT, glfwGetKey(win, GLFW_KEY_LEFT) == GLFW_PRESS); */
	/* nk_input_key(&Ctx->Nuklear, NK_KEY_RIGHT, glfwGetKey(win, GLFW_KEY_RIGHT) == GLFW_PRESS); */
	/* nk_input_key(&Ctx->Nuklear, NK_KEY_UP, glfwGetKey(win, GLFW_KEY_UP) == GLFW_PRESS); */
	/* nk_input_key(&Ctx->Nuklear, NK_KEY_DOWN, glfwGetKey(win, GLFW_KEY_DOWN) == GLFW_PRESS); */
	/* if (glfwGetKey(win, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS || */
	/*     glfwGetKey(win, GLFW_KEY_RIGHT_CONTROL) == GLFW_PRESS) { */
	/* 	nk_input_key(&Ctx->Nuklear, NK_KEY_COPY, glfwGetKey(win, GLFW_KEY_C) == GLFW_PRESS); */
	/* 	nk_input_key(&Ctx->Nuklear, NK_KEY_PASTE, glfwGetKey(win, GLFW_KEY_P) == GLFW_PRESS); */
	/* 	nk_input_key(&Ctx->Nuklear, NK_KEY_CUT, glfwGetKey(win, GLFW_KEY_X) == GLFW_PRESS); */
	/* 	nk_input_key(&Ctx->Nuklear, NK_KEY_CUT, glfwGetKey(win, GLFW_KEY_E) == GLFW_PRESS); */
	/* 	nk_input_key(&Ctx->Nuklear, NK_KEY_SHIFT, 1); */
	/* } else { */
	/* 	nk_input_key(&Ctx->Nuklear, NK_KEY_COPY, 0); */
	/* 	nk_input_key(&Ctx->Nuklear, NK_KEY_PASTE, 0); */
	/* 	nk_input_key(&Ctx->Nuklear, NK_KEY_CUT, 0); */
	/* 	nk_input_key(&Ctx->Nuklear, NK_KEY_SHIFT, 0); */
	/* } */
	nk_input_motion(&Ctx->Nuklear, Input->MouseX, Input->MouseY);
	nk_input_button(&Ctx->Nuklear, NK_BUTTON_LEFT, Input->MouseX, Input->MouseY, Input->MouseDown);
	/* nk_input_button(&Ctx->Nuklear, NK_BUTTON_MIDDLE, Input->MouseX, Input->MouseY, Input->MouseDown); */
	/* nk_input_button(&Ctx->Nuklear, NK_BUTTON_RIGHT, Input->MouseX, Input->MouseY, Input->MouseDown); */
	nk_input_end(&Ctx->Nuklear);
}

void InitNuklear(nuklear_context *Ctx, memory_index MemSize, void *Memory)
{
#define PERM_SIZE 500 KB
#define MIN_TMP_SIZE 250 KB

	assert(PERM_SIZE + MIN_TMP_SIZE < MemSize);

	InitializeListAllocator(&Ctx->ListAlloc, PERM_SIZE, Memory);
	Ctx->PermAlloc.userdata = nk_handle_ptr(&Ctx->ListAlloc);
	Ctx->PermAlloc.alloc    = (nk_plugin_alloc) NuklearPermAlloc;
	Ctx->PermAlloc.free     = (nk_plugin_free)  NuklearPermFree;

	InitializeArena(&Ctx->ArenaAlloc, PTROFFSET(Memory, PERM_SIZE),
			MemSize - PERM_SIZE);
	Ctx->TempAlloc.userdata = nk_handle_ptr(&Ctx->ArenaAlloc);
	Ctx->TempAlloc.alloc    = (nk_plugin_alloc) NuklearTempAlloc;
	Ctx->TempAlloc.free     = (nk_plugin_free)  NuklearTempFree;

	InitDevice(&Ctx->Graphics, &Ctx->PermAlloc);
	check_errors("nuklear init device");
	InitFont(&Ctx->Font, &Ctx->Graphics, &Ctx->PermAlloc, &Ctx->TempAlloc);
	check_errors("nuklear init font");
	FreeAll(&Ctx->ArenaAlloc);

	assert(nk_init(&Ctx->Nuklear,
		       &Ctx->PermAlloc,
		       &Ctx->Font->handle));
}

void UploadBufferData(struct nk_context *Nuklear, device *Graphics,
		      enum nk_anti_aliasing AA)
{
	glBindBuffer(GL_ARRAY_BUFFER,         Graphics->VertexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Graphics->IndexBuffer);

	/* load draw vertices & elements directly into vertex + element buffer */
	void *Vertices = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	void *Elements = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
	/* fill convert configuration */
	struct nk_convert_config Config = {0};
	static const struct nk_draw_vertex_layout_element VertexLayout[] = {
		{NK_VERTEX_POSITION, NK_FORMAT_FLOAT,
		 NK_OFFSETOF(struct nk_glfw_vertex, Position)},
		{NK_VERTEX_TEXCOORD, NK_FORMAT_FLOAT,
		 NK_OFFSETOF(struct nk_glfw_vertex, TexturePos)},
		{NK_VERTEX_COLOR, NK_FORMAT_R8G8B8A8,
		 NK_OFFSETOF(struct nk_glfw_vertex, Color)},
		{NK_VERTEX_LAYOUT_END}
	};
	Config.vertex_layout = VertexLayout;
	Config.vertex_size = sizeof(struct nk_glfw_vertex);
	Config.vertex_alignment = NK_ALIGNOF(struct nk_glfw_vertex);
	Config.null = Graphics->Nothing;
	Config.circle_segment_count = 22;
	Config.curve_segment_count = 22;
	Config.arc_segment_count = 22;
	Config.global_alpha = 1.0f;
	Config.shape_AA = AA;
	Config.line_AA = AA;

	/* setup buffers to load vertices and elements */
	struct nk_buffer VertexBuffer, IndexBuffer;
	nk_buffer_init_fixed(&VertexBuffer, Vertices, MAX_VERTEX_MEMORY);
	nk_buffer_init_fixed(&IndexBuffer, Elements, MAX_ELEMENT_MEMORY);

        /* this loads the data into the buffers */
	nk_convert(Nuklear, &Graphics->Commands, &VertexBuffer,
		   &IndexBuffer, &Config);

	glUnmapBuffer(GL_ARRAY_BUFFER);
	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
}

void DrawNuklear(nuklear_context *Ctx, int width, int height,
		 struct nk_vec2 scale, enum nk_anti_aliasing AA)
{
	device *Graphics = &Ctx->Graphics;
	GLfloat ortho[4][4] = {
		{2.0f, 0.0f, 0.0f, 0.0f},
		{0.0f,-2.0f, 0.0f, 0.0f},
		{0.0f, 0.0f,-1.0f, 0.0f},
		{-1.0f,1.0f, 0.0f, 1.0f},
	};
	ortho[0][0] /= (GLfloat)width;
	ortho[1][1] /= (GLfloat)height;

	/* setup global state */
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_SCISSOR_TEST);
	glActiveTexture(GL_TEXTURE0);

	/* setup program */
	glUseProgram(Graphics->Shader.Id);
	glUniform1i(Graphics->Shader.Texture, 0);
	glUniformMatrix4fv(Graphics->Shader.Projection, 1, GL_FALSE, &ortho[0][0]);
	/* convert from command queue into draw list and draw to screen */
	const struct nk_draw_command *Command;

	const nk_draw_index *Offset = NULL;

	/* allocate vertex and element buffer */
	glBindVertexArray(Graphics->ArrayBuffer);

	UploadBufferData(&Ctx->Nuklear, Graphics, AA);

	/* iterate over and execute each draw command */
	nk_draw_foreach(Command, &Ctx->Nuklear, &Graphics->Commands)
	{
		if (!Command->elem_count) continue;
		glBindTexture(GL_TEXTURE_2D, (GLuint)Command->texture.id);
		glScissor(
			(GLint)(Command->clip_rect.x * scale.x),
			(GLint)((height - (GLint)(Command->clip_rect.y + Command->clip_rect.h)) * scale.y),
			(GLint)(Command->clip_rect.w * scale.x),
			(GLint)(Command->clip_rect.h * scale.y));

		glDrawElements(GL_TRIANGLES, (GLsizei)Command->elem_count,
			       GL_UNSIGNED_SHORT, Offset);
		Offset += Command->elem_count;
	}
	nk_clear(&Ctx->Nuklear);
	nk_buffer_clear(&Graphics->Commands);

	/* default OpenGL state */
	glUseProgram(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glDisable(GL_BLEND);
	glDisable(GL_SCISSOR_TEST);
}

void ReleaseNuklear(nuklear_context *Ctx)
{

}
