#include <def.h>

EXTERN rect RectOfMWH(v2 Middle, f32 Width, f32 Height)
{
	rect Result;

	v2 Radius = V(Width/2, Height/2);

	Result.TopLeft = Middle - Radius;
	Result.BottomRight = Middle + Radius;
	
	return Result;
}

EXTERN rect RectOfTB(v2 TopLeft, v2 BottomRight)
{
	rect Result = {TopLeft, BottomRight};

	return Result;
}

EXTERN rect RectOfBounds(f32 Bottom, f32 Top, f32 Left, f32 Right)
{
	v2 TopLeft = V(Left, Top);
	v2 BottomRight = V(Right, Bottom);

	return RectOfTB(TopLeft, BottomRight);
}
