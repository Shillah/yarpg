#include <game/world.h>

#include <stdlib.h>
#include <stdio.h>

#include <util/arena_allocator.h>
#include <opengl/renderer.h>

static i32 HashOfChunk(iv3 Chunk)
{
	// replace with real hash
	i32 Result = 61 * Chunk.X +  149 * Chunk.Y + 7 * Chunk.Z;
	Result &= MAX_LOADED_CHUNKS - 1;
	// Result should now be between 0 (inc) and MAX_LOADED_CHUNKS (exc)
	return Result;
}

world_chunk *GetWorldChunk(world *World, iv3 Chunk)
{
	i32 Hash = HashOfChunk(Chunk);
	world_chunk *Current;
	i32 Collisions = 0;
	for (Current = World->LoadedChunks[Hash];
	     Current;
	     Current = Current->Next)
	{
		if (Current->Chunk == Chunk)
			break;
                Collisions += 1;
	}

	printf("GetWorldChunk: %d Hash collisions\n", Collisions);
	
	return Current;
}

static world_entity *GetEntityByIndex(world *World, i32 Index)
{
	YARPG_ASSERT(Index < (i32)ARRAYCOUNT(World->LoadedEntities));
	/* world_entity_list *LoadedEntities = &World->LoadedEntities; */
	world_entity *Result = NULL;

	/* while (LoadedEntities) */
	/* { */
	/* 	i32 NumEntities = LoadedEntities->NumEntities; */
	/* 	if (Index < NumEntities) */
	/* 	{ */
        /*                 Result = &LoadedEntities->Entities[Index]; */
	/* 		LoadedEntities = NULL; // break */
	/* 	} else { */
	/* 		Index -= NumEntities; */
	/* 		YARPG_ASSERT(ENTITIES_PER_BLOCK == NumEntities); */
	/* 		LoadedEntities = LoadedEntities->Next; */
	/* 	} */
	/* } */

	Result = &World->LoadedEntities[Index];

	return Result;
}

static void RegisterEntity(world_chunk *Chunk, i32 EntityIndex)
{
        YARPG_ASSERT(Chunk->NumEntities < ENTITIES_PER_BLOCK);
        if (Chunk->NumEntities < ENTITIES_PER_BLOCK)
	{
		Chunk->EntityIndices[Chunk->NumEntities++] = EntityIndex;
	} else {
		// ???
	}        
}

static void AddEntity(world *World, entity_type Type, world_position Position,
		      f32 Color[3], rect BoundingBox, world_chunk *Chunk)
{
	YARPG_ASSERT(Chunk);
	YARPG_ASSERT(Chunk->Chunk == Position.Chunk);
	i32 FreeEntityIndex = World->NumLoadedEntities++;
	world_entity *FreeEntity = GetEntityByIndex(World, FreeEntityIndex);
	FreeEntity->Type = Type;
	FreeEntity->Pos  = Position;
	FreeEntity->BoundingBox = BoundingBox;
	FreeEntity->Color[0] = Color[0];
	FreeEntity->Color[1] = Color[1];
	FreeEntity->Color[2] = Color[2];
	RegisterEntity(Chunk, FreeEntityIndex);
}

static void AddEntity(world *World, entity_type Type, world_position Position,
		      f32 Color[3], rect BoundingBox)
// Type, Position, ... should probably become "Components" later.
{
	world_chunk *Chunk = GetWorldChunk(World, Position.Chunk);
	if (!Chunk)
	{
		Chunk = GenerateWorldChunk(World, Position.Chunk, PushStruct(World->Arena,
									     world_chunk));
	}
	YARPG_ASSERT(Chunk);
	AddEntity(World, Type, Position, Color, BoundingBox, Chunk);
}

static void AddTile(world *World, world_chunk *Chunk, v2 Offset, f32 Color[3])
{
        i32 FreeEntityIndex = World->NumLoadedEntities++;
	world_entity *FreeEntity = GetEntityByIndex(World, FreeEntityIndex);
	YARPG_ASSERT(FreeEntity);
	FreeEntity->Type = EntityType_BACKGROUND;
	FreeEntity->Pos.Chunk = Chunk->Chunk;
	FreeEntity->Pos.Offset = Offset;
	FreeEntity->BoundingBox = (rect) {
		.TopRight = V(+0.5, +0.5),
		.BottomLeft = V(-0.5, -0.5)
	};
	FreeEntity->Color[0] = Color[0];
	FreeEntity->Color[1] = Color[1];
	FreeEntity->Color[2] = Color[2];
        RegisterEntity(Chunk, FreeEntityIndex);
}

void AddCreature(world *World, world_position Pos, f32 Color[3])
{
        rect Box = (rect) {
		.TopRight = V(+0.5, +0.5),
		.BottomLeft = V(-0.5, -0.5)
	};
	
	AddEntity(World, EntityType_UNIT, Pos, Color, Box);
}

static void LoadActiveEntitiesFromChunk(world *World, world_chunk *Chunk)
{
	YARPG_ASSERT(World->NumActiveEntities < MAX_ACTIVE_ENTITIES);

	iv3 ActiveChunk = World->ActiveChunk;
	iv3 Offset = Chunk->Chunk - ActiveChunk;
	// for (/* NOTHING */; Chunk; Chunk = Chunk->Next)
	// {
                
	// }

        YARPG_ASSERT(Offset.Z == 0);

	for (i32 Entity = 0; Entity < Chunk->NumEntities; ++Entity)
	{
		i32 EntityIndex = Chunk->EntityIndices[Entity];
		active_world_entity *ActiveEntity = &World->ActiveEntities[World->NumActiveEntities++];
		ActiveEntity->BaseIndex = EntityIndex;
		ActiveEntity->RelPosition = World->LoadedEntities[EntityIndex].Pos.Offset +
			V((f32) Offset.X * CHUNK_DIM, (f32) Offset.Y * CHUNK_DIM);
	}
}

static void SetActive(world *World, iv3 Chunk)
{
	// load all entities in Chunk into the active entity list
	World->ActiveChunk = Chunk;
	iv3 ChunksToLoad[] = {
		IV(0, 0, 0) + Chunk,
		IV(1, 0, 0) + Chunk,
		IV(-1, 0, 0) + Chunk,
		IV(0, 1, 0) + Chunk,
		IV(0, -1, 0) + Chunk,
		IV(1, 1, 0) + Chunk,
		IV(-1, 1, 0) + Chunk,
                IV(1, -1, 0) + Chunk,
		IV(-1, -1, 0) + Chunk,
	};

        // unload all loaded entities
	World->NumActiveEntities = 0;
        
	for (i32 CurrentChunk = 0;
	     CurrentChunk < (i32) ARRAYCOUNT(ChunksToLoad);
	     ++CurrentChunk)
	{
		world_chunk *Current = GetWorldChunk(World, ChunksToLoad[CurrentChunk]);
		if (!Current)
		{
			world_chunk *NewChunk = PushStruct(World->Arena, world_chunk);                        
			GenerateWorldChunk(World, ChunksToLoad[CurrentChunk], NewChunk);
			Current = GetWorldChunk(World, ChunksToLoad[CurrentChunk]);
		}
		YARPG_ASSERT(Current);

                LoadActiveEntitiesFromChunk(World, Current);
	}
}

static void RandomiseTiles(world *World, world_chunk *Chunk)
{
	v2 Offset;
	f32 Color[3];
	f32 BWColor[2][3] = {{0.0f, 0.0f, 0.0f},
			     {1.0f, 1.0f, 1.0f}};
	v2 Middle = V((f32)(CHUNK_DIM/2), (f32) (CHUNK_DIM/2));
	for (i32 Y = 0;
	     Y < CHUNK_DIM;
	     ++Y)
	{
		for (i32 X = 0;
		     X < CHUNK_DIM;
		     ++X)
		{
                        Offset = V((f32)X, (f32)Y) - Middle;
			Color[0] = (f32)rand()/(f32)(RAND_MAX);
			Color[1] = (f32)rand()/(f32)(RAND_MAX);
			Color[2] = (f32)rand()/(f32)(RAND_MAX);
			i32 Index = (Chunk->Chunk.X + Chunk->Chunk.Y + X + Y) % 2;
			AddTile(World, Chunk, Offset, BWColor[Index == 0 ? 0 : 1]);
		}
	}
}

world_chunk *GenerateWorldChunk(world *World, iv3 Chunk, world_chunk *NewChunk)
{
        printf("Generating Chunk: (%d, %d, %d)\n",
	       Chunk.X, Chunk.Y, Chunk.Z);
	i32 Hash = HashOfChunk(Chunk);
	NewChunk->Chunk = Chunk;
	RandomiseTiles(World, NewChunk);
	NewChunk->Next = World->LoadedChunks[Hash];
	World->LoadedChunks[Hash] = NewChunk;

	return NewChunk;
}

void InitWorld(world *World, memory_arena *Arena)
{
	World->NumLoadedEntities = 0;
	World->FreeChunks = NULL;

	World->Arena = Arena;

        iv3 InitChunks[] = {
		IV(0, 0, 0),
		IV(1, 0, 0),
		IV(-1, 0, 0),
		IV(0, 1, 0),
		IV(0, -1, 0),
		IV(1, 1, 0),
		IV(-1, 1, 0),
                IV(1, -1, 0),
		IV(-1, -1, 0),
	};

	for (i32 i = 0;
	     i < (i32) ARRAYCOUNT(InitChunks);
	     ++i)
	{
		GenerateWorldChunk(
			World,
			InitChunks[i],
			PushStruct(Arena, world_chunk));
	}
	World->CameraPos = {InitChunks[0], V(0.0, 0.0)};
	SetActive(World, InitChunks[0]);
}

void DrawWorld(world *World, renderer *Renderer)
{
	struct vertex_data Vertices[4];
	v3 Transform = V(0, 0, 1);        
	
	// for (i32 i = 0; i < World->NumLoadedEntities; ++i)
	// {
	// 	world_entity *Current = &World->LoadedEntities[i];
	// 	world_position Middle = Current->Pos;
	// 	rect Box = Current->BoundingBox;
	// 	f32 X = (f32) Middle.Chunk.X * CHUNK_DIM + Middle.Offset.X;
	// 	f32 Y = (f32) Middle.Chunk.Y * CHUNK_DIM + Middle.Offset.Y;
	// 	v3 MiddlePos   = V(X, Y, 0);
	// 	v3 TopLeft     = V(Box.TopLeft.X, Box.TopLeft.Y, 0);
	// 	v3 BottomRight = V(Box.BottomRight.X, Box.BottomRight.Y, 0);
	// 	v3 TopRight    = V(Box.BottomRight.X, Box.TopLeft.Y, 0);
	// 	v3 BottomLeft  = V(Box.TopLeft.X, Box.BottomRight.Y, 0);
        //         v4 Color       = V(Current->Color[0], Current->Color[1],
	// 			   Current->Color[2], 1);
	// 	Vertices[0] = {MiddlePos+TopLeft, Transform, Color};
	// 	Vertices[1] = {MiddlePos+BottomLeft, Transform, Color};
	// 	Vertices[2] = {MiddlePos+TopRight, Transform, Color};
	// 	Vertices[3] = {MiddlePos+BottomRight, Transform, Color};
		
	// 	DrawTriangleStrip(Renderer, 4, Vertices);
	// }

	for (i32 ActiveIndex = 0;
	     ActiveIndex < World->NumActiveEntities;
	     ++ActiveIndex)
	{
		active_world_entity *Entity = &World->ActiveEntities[ActiveIndex];
		world_entity *Base = &World->LoadedEntities[Entity->BaseIndex];                
		v2 RelPos = Entity->RelPosition;
		rect Box = Base->BoundingBox;
		v3 MiddlePos   = V(RelPos.X, RelPos.Y, 0);
		v3 TopRight     = V(Box.TopRight.X, Box.TopRight.Y, 0);
		v3 BottomLeft = V(Box.BottomLeft.X, Box.BottomLeft.Y, 0);
		v3 TopLeft    = V(Box.BottomLeft.X, Box.TopRight.Y, 0);
		v3 BottomRight  = V(Box.TopRight.X, Box.BottomLeft.Y, 0);
                v4 Color       = V(Base->Color[0], Base->Color[1],
				   Base->Color[2], 1);
		Vertices[0] = {MiddlePos+TopLeft, Transform, Color};
		Vertices[1] = {MiddlePos+BottomLeft, Transform, Color};
		Vertices[2] = {MiddlePos+TopRight, Transform, Color};
		Vertices[3] = {MiddlePos+BottomRight, Transform, Color};
		
		DrawTriangleStrip(Renderer, 4, Vertices);
	}
}

void CanonizePosition(world_position *Position)
{
	f32 Threshold = CHUNK_DIM / 2.0f;        
	if (Position->Offset.Y < -Threshold)
	{
		do
		{
			Position->Offset.Y += CHUNK_DIM;
			Position->Chunk.Y  -= 1;
		} while (Position->Offset.Y < -Threshold);
	} else if (Position->Offset.Y > Threshold)
	{
		do
		{
			Position->Offset.Y -= CHUNK_DIM;
			Position->Chunk.Y  += 1;
		} while (Position->Offset.Y > Threshold);
	}
	if (Position->Offset.X < -Threshold)
	{
		do
		{
			Position->Offset.X += CHUNK_DIM;
			Position->Chunk.X  -= 1;
		} while (Position->Offset.X < -Threshold);
	} else if (Position->Offset.X > Threshold)
	{
		do
		{
			Position->Offset.X -= CHUNK_DIM;
			Position->Chunk.X  += 1;
		} while (Position->Offset.X > Threshold);
	}
}

void SetCameraPos(world *World, world_position CameraPos)
{
	if (World->CameraPos.Chunk != CameraPos.Chunk)
	{
		printf("Changed!\n");
		SetActive(World, CameraPos.Chunk);
	}
	World->CameraPos = CameraPos;
}
