#include <opengl/renderer.h>

#include <assert.h>

void RendererCreate(renderer *Renderer, program *Shader)
{
	glGenBuffers(2, &Renderer->StreamVBO);
	glGenVertexArrays(1, &Renderer->StreamVAO);

	glBindVertexArray(Renderer->StreamVAO);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer->StreamVBO);
	glBufferData(GL_ARRAY_BUFFER,
		     BATCH_VERTEX_SIZE * sizeof(vertex_data),
		     NULL, GL_DYNAMIC_DRAW);


	glVertexAttribPointer(Shader->Position, 3, GL_FLOAT, GL_FALSE,
			      sizeof(vertex_data),
			      (void const *) offsetof(vertex_data, Position));
	glVertexAttribPointer(Shader->Color, 4, GL_FLOAT, GL_FALSE,
			      sizeof(vertex_data),
			      (void const *) offsetof(vertex_data, Color));
	glVertexAttribPointer(Shader->Transform, 3, GL_FLOAT, GL_FALSE,
			      sizeof(vertex_data),
			      (void const *) offsetof(vertex_data, Transform));

	glEnableVertexAttribArray(Shader->Position);
	glEnableVertexAttribArray(Shader->Color);
	glEnableVertexAttribArray(Shader->Transform);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Renderer->StreamIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		     BATCH_VERTEX_SIZE * sizeof(u32),
		     NULL, GL_DYNAMIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	Renderer->Shader = Shader;
}

void RendererPrepare(renderer *Renderer, memory_arena *Arena)
{
	Renderer->Arena = Arena;
	Renderer->First = PushStruct(Arena, render_batch);
	Renderer->Last  = Renderer->First;
	Renderer->First->Free = BATCH_VERTEX_SIZE;
	Renderer->First->Next = NULL;
}

static void AddTriangles(renderer *Renderer,
			 u32 NumVertices, vertex_data Data[],
			 u32 NumIndices,  u32 Indices[])
{
	// NumVertices should always be smaller than NumIndices
	assert(NumVertices <= NumIndices);
	u32 NumData = NumIndices;
	assert(NumData < BATCH_VERTEX_SIZE);
	render_batch *Current = Renderer->Last;
	if (Current->Free < NumData)
	{
		// create a new batch
		// TODO: search in the batch list for a suitable one
		render_batch *Next = PushStruct(Renderer->Arena,
						render_batch);
		assert(Next != NULL);
		Current->Next = Next;
		Next->Next = NULL;
		Renderer->Last = Next;
		Next->Free = BATCH_VERTEX_SIZE;
		Current = Next;
	}
	assert(NumData < Current->Free);
	u32 Start = BATCH_VERTEX_SIZE - Current->Free;
	Current->Free -= NumData;
        for (u32 i = 0; i < NumVertices; ++i)
	{
                Current->Data[Start+i] = Data[i];
	}
	for (u32 i = 0; i < NumIndices; ++i)
	{
                Current->Indices[Start+i] = Indices[i] + Start;
	}
}

void DrawTriangles(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data)
{
	if (NumVertices >= 3)
	{
		u32 NumTriangles = NumVertices/3;
		u32 NumIndices = NumTriangles * 3;
		u32 First = 0, Second = 1, Third = 2;

                u32 (*Indices)[3] = (u32 (*)[3]) PushArray(Renderer->Arena, NumTriangles * 3, f32);
                for (u32 CurrentTriangle = 0;
		     CurrentTriangle < NumTriangles;
		     ++CurrentTriangle)
		{
			DURING_DEBUG(
				assert(First < NumVertices);
				assert(Second < NumVertices);
				assert(Third < NumVertices);
				);
			Indices[CurrentTriangle][0] = First;
			Indices[CurrentTriangle][1] = Second;
			Indices[CurrentTriangle][2] = Third;
			First  = Third + 1;
			Second = First + 1;
			Third  = Second + 1;
		}
		AddTriangles(Renderer, NumVertices, Data,
			     NumIndices, (u32 *) Indices);
	}
}

void DrawTriangleElements(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data,
	u32 NumIndices,
	u32 *Indices)
{
	if (NumVertices >= 3)
	{
		AddTriangles(Renderer, NumVertices, Data,
			     NumIndices, Indices);
	}
}

void DrawTriangleStrip(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data
	)
{
	if (NumVertices >= 3)
	{
		u32 NumTriangles = NumVertices - 2;
		u32 NumIndices = 3 * NumTriangles;
		u32 First = 0, Second = 1, Third = 2;
		u32 (*Indices)[3] = (u32 (*)[3]) PushArray(Renderer->Arena, NumTriangles * 3, f32);
                for (u32 CurrentTriangle = 0;
		     CurrentTriangle < NumTriangles;
		     ++CurrentTriangle)
		{

			DURING_DEBUG(
				assert(First < NumVertices);
				assert(Second < NumVertices);
				assert(Third < NumVertices);
				);
			Indices[CurrentTriangle][0] = First;
			Indices[CurrentTriangle][1] = Second;
			Indices[CurrentTriangle][2] = Third;
			First = Second;
			Second = Third;
			Third  = Third + 1;
		}
		AddTriangles(Renderer, NumVertices, Data,
			     NumIndices, (u32 *) Indices);
	}
}

void DrawTriangleFan(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data
	)
{
	if (NumVertices >= 3)
	{
		u32 NumTriangles = NumVertices - 2;
		u32 NumIndices = 3 * NumTriangles;
		u32 First = 0, Second = 1, Third = 2;
		u32 (*Indices)[3] = (u32 (*)[3]) PushArray(Renderer->Arena, NumTriangles * 3, f32);
                for (u32 CurrentTriangle = 0;
		     CurrentTriangle < NumTriangles;
		     ++CurrentTriangle)
		{
			DURING_DEBUG(
				assert(First < NumVertices);
				assert(Second < NumVertices);
				assert(Third < NumVertices);
				);
			Indices[CurrentTriangle][0] = First;
			Indices[CurrentTriangle][1] = Second;
			Indices[CurrentTriangle][2] = Third;
			Second = Third;
			Third  = Third + 1;
		}
		AddTriangles(Renderer, NumVertices, Data,
			     NumIndices, (u32 *) Indices);
	}
}

void RendererDraw(renderer *Renderer,
		  GLint ProjectionLocation,
                  f32 Projection[])
{
	glUseProgram(Renderer->Shader->Id);
	glBindVertexArray(Renderer->StreamVAO);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer->StreamVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Renderer->StreamIBO);
	glUniformMatrix4fv(
		ProjectionLocation,
		1, GL_TRUE, Projection
		);
	for (render_batch *Current = Renderer->First;
	     Current != NULL;
	     Current = Current->Next)
	{
		u32 Used = BATCH_VERTEX_SIZE - Current->Free;
		if (Used == 0)
			continue;
                assert(Used < BATCH_VERTEX_SIZE);
		glBufferSubData(
			GL_ARRAY_BUFFER, 0,
			Used * sizeof(vertex_data),
			Current->Data);
		glBufferSubData(
			GL_ELEMENT_ARRAY_BUFFER, 0,
			Used * sizeof(u32),
			Current->Indices);
		glDrawElements(
			GL_TRIANGLES,
			Used,
			GL_UNSIGNED_INT,
			NULL);
	}
}


void RendererRelease(renderer *Renderer)
{
	glDeleteBuffers(2, &Renderer->StreamVBO);
	glDeleteVertexArrays(1, &Renderer->StreamVAO);
}

void DrawTexturedTriangles(
	renderer *Renderer,
	u32 NumVertices,
	vertex_data *Data,
	GLuint Texture
	)
{

}
