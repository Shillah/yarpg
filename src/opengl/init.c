#include <opengl/init.h>

GLFWwindow *init(opengl options, char const *title, int width, int height)
{
	glfwInit();
        
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, options.major);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, options.minor);
	glfwWindowHint(GLFW_OPENGL_PROFILE,        options.profile);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, options.forward_compat);
	glfwWindowHint(GLFW_SAMPLES, options.anti_alias);
  
	glfwWindowHint(GLFW_RESIZABLE,             options.resizable);

	GLFWmonitor *monitor = options.fullscreen ? glfwGetPrimaryMonitor() : NULL;
	
	GLFWwindow* window = glfwCreateWindow(width, height, title, monitor, NULL);
        
	glfwMakeContextCurrent(window);

	// this has to be called _after_ the opengl context has been created
	// i.e. after glfwMakeContextCurrent
	glewExperimental = GL_TRUE;
	glewInit();

	return window;
}

void terminate(GLFWwindow *w)
{
	glfwDestroyWindow(w);
	glfwTerminate();
}
