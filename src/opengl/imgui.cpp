#include <opengl/imgui.h>

#include <def.h>

#include <stdio.h>
#include <assert.h>
#include <cstring>

#define WNDCOL_ACTIVE 0.2f
#define WNDCOL_HOT    0.1f
#define WNDCOL_COLD   0.05f

#define WGTCOL_ACTIVE 0.9f
#define WGTCOL_HOT    0.75f
#define WGTCOL_COLD   0.6f
#define WGTCOL_BKG    0.35f

//#include <opengl/string.h>

static b32 InsideIRect(irect Rect, iv2 Point)
{

	b32 XWorks = Rect.BottomLeft.X <= Point.X && Point.X <= Rect.TopRight.X;
	b32 YWorks = Rect.BottomLeft.Y <= Point.Y && Point.Y <= Rect.TopRight.Y;
	b32 Result = XWorks && YWorks;
	return Result;
}

static void DrawIRect(ui_state *UiState, irect Rect, f32 Color[3])
{
	i32 X = Rect.BottomLeft.X;
	i32 Y = Rect.BottomLeft.Y;
	i32 Width = Rect.TopRight.X - X;
	i32 Height = Rect.TopRight.Y - Y;

	X += UiState->CurrentBox.BottomLeft.X;
	Y += UiState->CurrentBox.BottomLeft.Y;

	f32 ScreenHalfWidth = UiState->FrameWidth / 2.0f;
	f32 ScreenHalfHeight = UiState->FrameHeight / 2.0f;
	f32 OnScreenX = ((f32) X - ScreenHalfWidth) / ScreenHalfWidth;
	f32 OnScreenY = ((f32) ScreenHalfHeight - Y) / ScreenHalfHeight;
	f32 OnScreenWidth = (f32) Width / ScreenHalfWidth;
	f32 OnScreenHeight = (f32) Height / ScreenHalfHeight;

	vertex_data Vertices[4] = {
		{{OnScreenX, OnScreenY, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color[0], Color[1], Color[2], 1.0}},
		{{OnScreenX + OnScreenWidth, OnScreenY, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color[0], Color[1], Color[2], 1.0}},
		{{OnScreenX, OnScreenY - OnScreenHeight, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color[0], Color[1], Color[2], 1.0}},
		{{OnScreenX + OnScreenWidth, OnScreenY - OnScreenHeight, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color[0], Color[1], Color[2], 1.0}},
	};

	DrawTriangleStrip(
		&UiState->Renderer,
		4, Vertices);
}

static b32 IsHotWidget(ui_state *UiState, irect Box)
{
	iv2 Mouse = {UiState->MouseX, UiState->MouseY};
	return (UiState->HotWindow == UiState->CurrentWindow) &&
		InsideIRect(Box, Mouse - UiState->HotBox.BottomLeft);
}

static b32 TestAndSetHotWidget(ui_state *UiState, i32 WidgetId, irect Box)
{
	b32 Result = IsHotWidget(UiState, Box);
	if (Result)
	{
		UiState->HotWidget = WidgetId;
	}
	return Result;
}

static b32 IsActiveWidget(ui_state *UiState, irect Box)
{
	iv2 Mouse = {UiState->MouseX, UiState->MouseY};
	return (UiState->ActiveWindow == UiState->CurrentWindow) &&
		UiState->MouseDown &&
		UiState->ActiveWidget == 0 &&
		InsideIRect(Box, Mouse - UiState->ActiveBox.BottomLeft);
}

static b32 TestAndSetActiveWidget(ui_state *UiState, i32 WidgetId, irect Box)
{
	b32 ShouldBeActive = IsActiveWidget(UiState, Box);
	b32 WasActive = (UiState->ActiveWindow == UiState->CurrentWindow) &&
		(UiState->ActiveWidget == WidgetId);
	b32 Result = WasActive || ShouldBeActive;
	if (Result)
	{
		UiState->ActiveWidget = WidgetId;
	}
	return Result;
}

static b32 IsHotWindow(ui_state *UiState, irect Box)
{
	iv2 Mouse = {UiState->MouseX, UiState->MouseY};
	return (UiState->HotWindow == 0) &&
		InsideIRect(Box, Mouse);
}

static void SetCurrentWindowHot(ui_state *UiState)
{
	assert(!UiState->HotWindow);
	UiState->HotWindow = UiState->CurrentWindow;
	UiState->HotBox  = UiState->CurrentBox;
}

static b32 TestAndSetHotWindow(ui_state *UiState)
{
	b32 Result = IsHotWindow(UiState, UiState->CurrentBox);
	if (Result)
	{
		SetCurrentWindowHot(UiState);
	}
	return Result;
}

static b32 IsActiveWindow(ui_state *UiState, irect Box)
{
	iv2 Mouse = {UiState->MouseX, UiState->MouseY};
	return (UiState->ActiveWindow == 0) &&
		UiState->MouseDown &&
		InsideIRect(Box, Mouse);
}

static void SetCurrentWindowActive(ui_state *UiState)
{
	assert(!UiState->ActiveWindow || UiState->ActiveWindow == UiState->CurrentWindow);
	UiState->ActiveWindow = UiState->CurrentWindow;
	UiState->ActiveBox  = UiState->CurrentBox;
}

static b32 TestAndSetActiveWindow(ui_state *UiState)
{
	b32 ShouldBeActive = IsActiveWindow(UiState, UiState->CurrentBox);
	b32 WasActive = UiState->ActiveWindow == UiState->CurrentWindow;
	b32 Result = WasActive || ShouldBeActive;
	if (Result)
	{
		SetCurrentWindowActive(UiState);
	}

	return Result;
}

static void SetCurrentWindow(ui_state *UiState, i32 WindowId, irect Box)
{
	assert(!UiState->CurrentWindow);
	UiState->CurrentWindow = WindowId;
	UiState->CurrentBox    = Box;
	TestAndSetHotWindow(UiState);
	TestAndSetActiveWindow(UiState);
}

void InitGui(ui_state *UiState,
	     string_state *StringState,
	     program *Shader,
	     i32 FrameHeight, i32 FrameWidth)
{
	UiState->FrameHeight = FrameHeight;
	UiState->FrameWidth = FrameWidth;
	UiState->ActiveWidget = 0;
	UiState->HotWidget = 0;
	UiState->HotWindow = 0;
	UiState->ActiveWindow = 0;
	UiState->StringState = StringState;

	// TODO: give the UI its own Shader
//	program Shader = make_program(,,,,);

	RendererCreate(&UiState->Renderer, Shader);
}

void PrepareFrame(ui_state *UiState,
		  memory_arena *Arena,
		  i32 MouseX, i32 MouseY, i32 MouseDown)
{
        UiState->MouseX = MouseX;
	UiState->MouseY = MouseY;
	UiState->MouseDown = MouseDown;


	if (!MouseDown)
	{
		UiState->ActiveWindow = 0;
		UiState->ActiveWidget = 0;
	}
	// if the mouse is down, but no item is active, we shouldnt
	// ensure that no item can be activated.
	// this prevents situations where we accidentally click a button
	// when we hover over it while having our mouse down.
	else if (UiState->HotWindow == 0 && UiState->ActiveWindow == 0)
	{
		UiState->ActiveWindow = -1;

	} else if (UiState->HotWidget == 0 && UiState->ActiveWidget == 0)
	{
		UiState->ActiveWidget = -1;
	}

	UiState->HotWidget = 0;
	UiState->HotWindow  = 0;
	RendererPrepare(&UiState->Renderer, Arena);
}

void FinishFrame(ui_state *UiState)
{
	f32 Projection[] = {
		1.0, 0.0, 0.0, 0.0,
		0.0, 1.0, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		0.0, 0.0, 0.0, 1.0
	};
        RendererDraw(&UiState->Renderer, 0, Projection);
}

b32 Button(ui_state *UiState,
	   i32 Id, i32 X, i32 Y, i32 Width, i32 Height)
{
	f32 Color = 0.2f;
	// we should actually check wether we are hot / active
	// and if yes set ourselves to that
	i32 Hot = 0, Active = UiState->ActiveWidget == Id, WasClicked = 0;

	if (!UiState->HotWidget &&
	    ((X <= UiState->MouseX) && (UiState->MouseX <= X + Width)) &&
	    ((Y <= UiState->MouseY) && (UiState->MouseY <= Y + Height)))
	{
		// we are hot!
		UiState->HotWidget = Id;
		Hot = 1;
		if (UiState->MouseDown && !UiState->ActiveWidget)
		{
			if (!Active) WasClicked = 1;
			UiState->ActiveWidget = Id;
			Active = 1;
		}
	}


	if (Active)
	{
		Color = 1.0f;
	} else if (Hot)
	{
		Color = 0.7f;
	}

	f32 ScreenHalfWidth = UiState->FrameWidth / 2.0f;
	f32 ScreenHalfHeight = UiState->FrameHeight / 2.0f;
	f32 OnScreenX = ((f32) X - ScreenHalfWidth) / ScreenHalfWidth;
	f32 OnScreenY = ((f32) ScreenHalfHeight - Y) / ScreenHalfHeight;
	f32 OnScreenWidth = (f32) Width / ScreenHalfWidth;
	f32 OnScreenHeight = (f32) Height / ScreenHalfHeight;
	vertex_data Vertices[4] = {
		{{OnScreenX, OnScreenY, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color, Color, Color, 1.0}},
		{{OnScreenX + OnScreenWidth, OnScreenY, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color, Color, Color, 1.0}},
		{{OnScreenX, OnScreenY - OnScreenHeight, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color, Color, Color, 1.0}},
		{{OnScreenX + OnScreenWidth, OnScreenY - OnScreenHeight, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color, Color, Color, 1.0}},
	};

	DrawTriangleStrip(
		&UiState->Renderer,
		4, Vertices);

	return WasClicked;
}

b32 Slider(ui_state *UiState,
	   i32 Id, i32 X, i32 Y, i32 Width, i32 Height,
	   f32 *Val, f32 Min, f32 Max)
{
        YARPG_ASSERT(UiState->CurrentWindow);
        f32 Current = *Val;
	assert(Min <= Current && Current <= Max);
	f32 Percent = (Current - Min) / (Max - Min);

	// we should actually check wether we are hot / active
	// and if yes set ourselves to that
	// i32 Hot = 0, Active = UiState->ActiveWidget == Id, WasChanged = 0;

	// // since we draw everything with doubled width at the moment,
	// // we need to change this check to reflect that fact!
	// if (!UiState->HotWidget &&
	//     ((X <= UiState->MouseX) && (UiState->MouseX <= X + Width)) &&
	//     ((Y <= UiState->MouseY) && (UiState->MouseY <= Y + Height)))
	// {
	// 	// we are hot!
	// 	UiState->HotWidget = Id;
	// 	Hot = 1;
	// 	if (UiState->MouseDown && !UiState->ActiveWidget)
	// 	{
	// 		UiState->ActiveWidget = Id;
	// 		Active = 1;
	// 	}
	// }

	irect Box = {.TopRight = IV(X+Width, Y+Height), .BottomLeft = IV(X, Y)};
	b32 Hot = TestAndSetHotWidget(UiState, Id, Box);
	b32 Active = TestAndSetActiveWidget(UiState, Id, Box);
	b32 WasChanged = 0;

	X += UiState->CurrentBox.BottomLeft.X;
	Y += UiState->CurrentBox.BottomLeft.Y;

	f32 Color;
	if (Active)
	{
		Color = WGTCOL_ACTIVE;
	} else if (Hot)
	{
		Color = WGTCOL_HOT;
	} else {
		Color = WGTCOL_COLD;
	}

	if (Active)
	{
                f32 Direction = ((f32) UiState->MouseX - X) / Width;
                if (Direction < 0.0)
		{
			Direction = 0.0;
		} else if (Direction > 1.0)
		{
			Direction = 1.0;
		}

		Percent = Direction;
		*Val = Percent * (Max - Min) + Min;
		WasChanged = 1;
	}

	f32 ScreenHalfWidth = UiState->FrameWidth / 2.0f;
	f32 ScreenHalfHeight = UiState->FrameHeight / 2.0f;
	f32 OnScreenX = ((f32) X - ScreenHalfWidth) / ScreenHalfWidth;
	f32 OnScreenY = ((f32) ScreenHalfHeight - Y) / ScreenHalfHeight;
	f32 OnScreenWidth = (f32) Width / ScreenHalfWidth;
	f32 OnScreenHeight = (f32) Height / ScreenHalfHeight;

	// since the slider has 1/10th of the thickness of the background
	// we should let SliderX move between 0->0.9 instead of 0->1
	f32 SliderX = OnScreenWidth * 0.9 * Percent + OnScreenX;
	f32 SliderWidth = 0.1 * OnScreenWidth;

	vertex_data BackgroundVertices[4] = {
		{{OnScreenX, OnScreenY, 0.0},
		 {0.0, 0.0, 1.0},
		 {WGTCOL_BKG, WGTCOL_BKG, WGTCOL_BKG, 1.0}},
		{{OnScreenX + OnScreenWidth, OnScreenY, 0.0},
		 {0.0, 0.0, 1.0},
		 {WGTCOL_BKG, WGTCOL_BKG, WGTCOL_BKG, 1.0}},
		{{OnScreenX, OnScreenY - OnScreenHeight, 0.0},
		 {0.0, 0.0, 1.0},
		 {WGTCOL_BKG, WGTCOL_BKG, WGTCOL_BKG, 1.0}},
		{{OnScreenX + OnScreenWidth, OnScreenY - OnScreenHeight, 0.0},
		 {0.0, 0.0, 1.0},
		 {WGTCOL_BKG, WGTCOL_BKG, WGTCOL_BKG, 1.0}},
	};



	vertex_data SliderVertices[4] = {
		{{SliderX, OnScreenY, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color, Color, Color, 1.0}},
		{{SliderX + SliderWidth, OnScreenY, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color, Color, Color, 1.0}},
		{{SliderX, OnScreenY - OnScreenHeight, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color, Color, Color, 1.0}},
		{{SliderX + SliderWidth, OnScreenY - OnScreenHeight, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color, Color, Color, 1.0}},
	};

	DrawTriangleStrip(
		&UiState->Renderer,
		4, BackgroundVertices);

	DrawTriangleStrip(
		&UiState->Renderer,
		4, SliderVertices);

	return WasChanged;
}

b32 Label(ui_state *UiState, i32 Id, i32 X, i32 Y, i32 Width, i32 Height,
	  char const *Content)
{
	return 0;
}

b32 TitleBar(ui_state *UiState, i32 Id, i32 X, i32 Y, i32 Width, i32 Height,
	     char const *Content)
{
        irect Box = {.TopRight = IV(X+Width, Y + Height), .BottomLeft = IV(X, Y)};
	f32  Color[3] = {0.03, 0.66, 0.05};
	DrawIRect(UiState, Box, Color);

	X += UiState->CurrentBox.BottomLeft.X;
	Y += UiState->CurrentBox.BottomLeft.Y;

	f32 ScreenHalfWidth = UiState->FrameWidth / 2.0f;
	f32 ScreenHalfHeight = UiState->FrameHeight / 2.0f;
	f32 OnScreenX = ((f32) X - ScreenHalfWidth) / ScreenHalfWidth;
	f32 OnScreenY = ((f32) ScreenHalfHeight - Y) / ScreenHalfHeight;
	f32 OnScreenWidth = (f32) Width / ScreenHalfWidth;
	f32 OnScreenHeight = (f32) Height / ScreenHalfHeight;

	// f32 Scale = OnScreenWidth > OnScreenHeight ? OnScreenHeight : OnScreenWidth;
	// Scale *= 5;

	f32 Scale = 1.0;
	
	f32 TextColor[] = {1.0, 0.0, 1.0};

//	printf("%d %d\n", X, Y);
	
	print_string(UiState->StringState, (f32) X, (f32) UiState->FrameHeight - Y, Scale, TextColor,
		     strlen(Content), Content);
	return 0;
}

b32 BeginWindow(ui_state *UiState,
		i32 Id,
		char const *Title,
		i32 X, i32 Y, i32 Width, i32 Height, b32 Flags)
{
	irect Box = {.TopRight = IV(X + Width, Y + Height), .BottomLeft = IV(X, Y)};
	SetCurrentWindow(UiState, Id, Box);

//	printf("%d %d %d\n", UiState->CurrentWindow, UiState->ActiveWindow, UiState->HotWindow);

	f32 Color;
	if (UiState->ActiveWindow == Id)
	{
		Color = WNDCOL_ACTIVE;
	} else if (UiState->HotWindow == Id) {
		Color = WNDCOL_HOT;
	} else {
		Color = WNDCOL_COLD;
	}


	f32 ScreenHalfWidth = UiState->FrameWidth / 2.0f;
	f32 ScreenHalfHeight = UiState->FrameHeight / 2.0f;
	f32 OnScreenX = ((f32) X - ScreenHalfWidth) / ScreenHalfWidth;
	f32 OnScreenY = ((f32) ScreenHalfHeight - Y) / ScreenHalfHeight;
	f32 OnScreenWidth = (f32) Width / ScreenHalfWidth;
	f32 OnScreenHeight = (f32) Height / ScreenHalfHeight;
	vertex_data Vertices[4] = {
		{{OnScreenX, OnScreenY, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color, Color, Color, 1.0}},
		{{OnScreenX + OnScreenWidth, OnScreenY, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color, Color, Color, 1.0}},
		{{OnScreenX, OnScreenY - OnScreenHeight, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color, Color, Color, 1.0}},
		{{OnScreenX + OnScreenWidth, OnScreenY - OnScreenHeight, 0.0},
		 {0.0, 0.0, 1.0},
		 {Color, Color, Color, 1.0}},
	};

	DrawTriangleStrip(
		&UiState->Renderer,
		4, Vertices);


	if (TitleBar(UiState, -2, 0, 0, Width, 10, Title))
	{

	}

	return 1;
}

b32 EndWindow(ui_state *UiState)
{
	UiState->CurrentWindow = 0;
	return 1;
}
