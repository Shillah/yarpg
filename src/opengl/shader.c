#include <opengl/shader.h>

#ifdef __cplusplus
#include <cassert>
#include <cstdlib>
#include <cstdio>
#else
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#endif

static void report_shader_error(char const *prefix,
				char const *suffix,
				GLuint shader)
{
	fputs("[", stderr);
	fputs(suffix, stderr);
	fputs(" SHADER COMPILE ERROR in ", stderr);
	if (prefix) {
                
		fputs(prefix, stderr);
                
	} else {
                fputs("NO NAME GIVEN", stderr);
	}
	fputs("]", stderr);        
	GLint length;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
	if (length > 0)
	{
		char buffer[length];
		glGetShaderInfoLog(shader, length, NULL, buffer);
		fputs(buffer, stderr);
		fputs("\n", stderr);
	}
}

static void report_shader_success(char const *prefix,
				  char const *suffix,
				  GLuint shader)
{
	fputs("[", stdout);
	fputs(suffix, stdout);
	fputs(" SHADER COMPILE SUCCESS in ", stdout);
	if (prefix) {
                
		fputs(prefix, stdout);
                
	} else {
                fputs("NO NAME GIVEN", stdout);
	}
	fputs("]", stdout);
	GLint length;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
	if (length > 0)
	{
		char buffer[length];
		glGetShaderInfoLog(shader, length, NULL, buffer);
		fputs(buffer, stdout);
		fputs("\n", stdout);
	}
}



static GLuint load_shader(GLenum type, char const *prefix, char const *source)
{
	GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE)
	{
		switch (type)
		{
			break;case GL_VERTEX_SHADER:   report_shader_error(prefix, "VERTEX", shader);
			break;case GL_FRAGMENT_SHADER: report_shader_error(prefix, "FRAGMENT", shader);
			break;case GL_GEOMETRY_SHADER: report_shader_error(prefix, "GEOMETRY", shader);
			break;default: report_shader_error(prefix, "(INTERNAL ERROR)", shader);
		}
		abort();
	}
	else
	{
		switch (type)
		{
			break;case GL_VERTEX_SHADER:   report_shader_success(prefix, "VERTEX", shader);
			break;case GL_FRAGMENT_SHADER: report_shader_success(prefix, "FRAGMENT", shader);
			break;case GL_GEOMETRY_SHADER: report_shader_success(prefix, "GEOMETRY", shader);
			break;default: report_shader_success(prefix, "(INTERNAL ERROR)", shader);
		}                
	}

	return shader;
}

static void report_program_error(char const *name, GLuint program)
{
	fputs("[PROGRAM LINK ERROR (", stderr);
	if (name) {
                
		fputs(name, stderr);
                
	} else {
                fputs("NO NAME GIVEN", stderr);
	}
	fputs(")]\n", stderr);

	GLint length;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
	if (length > 0)
	{
		char buffer[length];
		glGetProgramInfoLog(program, length, NULL, buffer);
		fputs(buffer, stderr);
		fputs("\n", stderr);
	}
}

static void report_program_success(char const *name, GLuint program)
{
	fputs("[PROGRAM LINK SUCCESS (", stdout);
	if (name) {
                
		fputs(name, stdout);
                
	} else {
                fputs("NO NAME GIVEN", stdout);
	}
	fputs(")]\n", stdout);
	GLint length;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
	if (length > 0)
	{
		char buffer[length];
		glGetProgramInfoLog(program, length, NULL, buffer);
		fputs(buffer, stdout);
		fputs("\n", stdout);
	}
}

program make_program(
	char const *name,
	char const *vertex_source,
	char const *geometry_source,
	char const *fragment_source)
{
	program Ret = {0};
	assert(vertex_source != NULL);
	assert(fragment_source != NULL);        
	
	GLuint vertex_shader, geometry_shader = 0, fragment_shader;

	vertex_shader = load_shader(GL_VERTEX_SHADER, name, vertex_source);
	fragment_shader = load_shader(GL_FRAGMENT_SHADER, name, fragment_source);

	if (geometry_source)
		geometry_shader = load_shader(GL_GEOMETRY_SHADER, name, geometry_source);
	
	Ret.Id = glCreateProgram();
	glAttachShader(Ret.Id, vertex_shader);
	if (geometry_shader)
		glAttachShader(Ret.Id, geometry_shader);
	glAttachShader(Ret.Id, fragment_shader);
	// Dont need this since we specify in the shader, that our
	// Color is at layout 0
	// I should probably check this again
//	glBindFragDataLocation(Ret.Id, 0, "outColor");
	glLinkProgram(Ret.Id);

	Ret.Position   = glGetAttribLocation(Ret.Id, "Position");
	Ret.TexturePos = glGetAttribLocation(Ret.Id, "TexturePos");
	Ret.Transform  = glGetAttribLocation(Ret.Id, "Transform");
	Ret.Color      = glGetAttribLocation(Ret.Id, "Color");

	Ret.Texture    = glGetUniformLocation(Ret.Id, "Texture");
	Ret.Projection = glGetUniformLocation(Ret.Id, "Projection");
	
	/* assert((Ret.Position == 0) */
	/*        && (Ret.TexturePos == 1) */
	/*        && (Ret.Transform == 2) */
	/*        && (Ret.Color == 3)); */

	assert(Ret.Position != -1);
	// those should probably be optional?
//	assert(Ret.TexturePos != -1);
//	assert(Ret.Transform != -1);
//	assert(Ret.Color != -1);
	GLint status;
	glGetProgramiv(Ret.Id, GL_LINK_STATUS, &status);

	if (status != GL_TRUE)
	{
		report_program_error(name, Ret.Id);
		abort();
	}

	glValidateProgram(Ret.Id);
        glGetProgramiv(Ret.Id, GL_VALIDATE_STATUS, &status);

	if (status != GL_TRUE)
	{
		report_program_error(name, Ret.Id);
		abort();
	}

	report_program_success(name, Ret.Id);

	// we should technically call glDetachShader first
	// but this can make debugging harder
	// since then the graphics card cannot refer to the shaders source
	// code.
#ifndef DEBUG
	glDetachShader(Ret.Id, vertex_shader);
	glDetachShader(Ret.Id, fragment_shader);
#endif
	
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
	return Ret;
}
