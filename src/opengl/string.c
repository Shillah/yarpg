#include <opengl/string.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <def.h>
#include <stdio.h>
#include <opengl/opengl.h>
#include <opengl/shader.h>

#include <util/file.h>
#include <assert.h>

static FT_Library library;
static FT_Face    face;

static char const font_name[] = FONT_NAME;
#define FIRST_CHAR 0
// tune down in case of stack overflow :(
#define LAST_CHAR 255

static f32 const OutlineWidth = 0.3f;

void string_setup(string_state *state)
{
	int error1 = 0, error2 = 0;
	if ((error1 = FT_Init_FreeType(&library)) ||
	    (error2 = FT_New_Face(library, font_name, 0, &face)))
	{
		if (error1)
		{
			puts("[FreeType]: Could not initialize FreeType");
		} else if (error2)
		{
			puts("[FreeType]: Failed to load font.");
		}
		return;
	}

	// IMPORTANT
	// MOST STUFF IS MEASURED IN 64ths of a pixel
	state->glyph_map.pt_size = 48;
	FT_Set_Pixel_Sizes(face, 0, state->glyph_map.pt_size);
	state->glyph_map.height = face->size->metrics.height;
	state->glyph_map.ascender = face->ascender;
	state->glyph_map.descender = face->descender;

	glGenTextures(1, &state->glyph_map.texture);
	glBindTexture(GL_TEXTURE_2D, state->glyph_map.texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	// TODO: find width & height without the loop
	u32 width = 0, height = 0;
	for (int i = FIRST_CHAR; i <= LAST_CHAR; ++i)
	{
		if (FT_Load_Char(face, (char)i, FT_LOAD_RENDER))
		{
			printf("Could not render char: %c\n", i);
		}
		else
		{
			width +=  face->glyph->bitmap.width;
			height = height > face->glyph->bitmap.rows ? height : face->glyph->bitmap.rows;
		}
	}
	state->glyph_map._texturew = width;
	state->glyph_map._textureh = height;
	// allocate texture
	glTexImage2D(
		GL_TEXTURE_2D, 0, GL_RED, width, height, 0,
		GL_RED, GL_UNSIGNED_BYTE, NULL);
	i32 x_off = 0;
	// Load chars 'a' - 'z'
	for (int i = FIRST_CHAR; i <= LAST_CHAR; ++i)
	{
		printf("Loading char: %d\n", i);
		if (FT_Load_Char(face, (char)i, FT_LOAD_RENDER))
		{
			printf("Could not render char: %c\n", i);
		}
		else
		{
			u32 glyph_height = face->glyph->bitmap.rows;
			u32 glyph_width  = face->glyph->bitmap.width;
			glTexSubImage2D(
				GL_TEXTURE_2D,
				0, x_off, 0,
                                glyph_width,
                                glyph_height,
				GL_RED,
				GL_UNSIGNED_BYTE,
				face->glyph->bitmap.buffer);
                        state->glyph_map._texcoords[i-FIRST_CHAR][0] = ((float) x_off)/width;
                        state->glyph_map._texcoords[i-FIRST_CHAR][1] =
                                ((float) x_off + face->glyph->bitmap.width)/width;
                        state->glyph_map._texcoords[i-FIRST_CHAR][2] =
                                ((float) face->glyph->bitmap.rows / height);
			state->glyph_map.size[i-FIRST_CHAR][0] = face->glyph->bitmap.width;
			state->glyph_map.size[i-FIRST_CHAR][1] = face->glyph->bitmap.rows;
			state->glyph_map.bearing[i-FIRST_CHAR][0] = face->glyph->bitmap_left;
			state->glyph_map.bearing[i-FIRST_CHAR][1] = face->glyph->bitmap_top;
			state->glyph_map.advance[i-FIRST_CHAR] = face->glyph->advance.x;
			x_off += glyph_width;
		}
	}

	char vertex[5000] = {0};
	char fragment[5000] = {0};

	FILE *f = open_file("res/shader/fstring.glsl");
	assert(f != NULL);
	u64 read = read_file(f, 0, 4999, fragment);
	assert(read < 4999);
	close_file(f);
	f = open_file("res/shader/vstring.glsl");
	assert(f != NULL);
	read = read_file(f, 0, 4999, vertex);
	assert(read < 4999);
	close_file(f);

	state->StringProgram = make_program("StringProgram", vertex, NULL, fragment);
	glGenVertexArrays(1, &state->vao);
	glGenBuffers(1, &state->vbo);
	glBindVertexArray(state->vao);
	glBindBuffer(GL_ARRAY_BUFFER, state->vbo);

	GLint vattrib = state->StringProgram.Position;
	glEnableVertexAttribArray(vattrib);
	glVertexAttribPointer(vattrib, 4, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	puts("Finished init.");

	FT_Done_Face(face);
	FT_Done_FreeType(library);
}

void print_string(
	string_state *state,
	f32 start_x, f32 start_y, f32 scale,
	f32 const Color[3],
	u32 size,
	char const *s)
{
	f32 x = start_x, y = start_y;
	glUseProgram(state->StringProgram.Id);

	GLint transformPos = glGetUniformLocation(state->StringProgram.Id, "transform");
	GLuint colorPos = glGetUniformLocation(state->StringProgram.Id, "textColor");

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(state->vao);
	glBindTexture(GL_TEXTURE_2D, state->glyph_map.texture);

	glBindBuffer(GL_ARRAY_BUFFER, state->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(f32[6][4]) * size,
		     NULL, GL_DYNAMIC_DRAW);

	void *VertexBufferData = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

	f32 (*vbd)[6][4] = VertexBufferData;

	u32 characters;
	u32 printed_characters = 0;
	for (characters = 0; characters < size && s[characters]; ++characters)
	{
		int c = (int) s[characters];
                if ((char) c != '\n')
		{
			f32 xpos = x + state->glyph_map.bearing[c][0] * scale;
			f32 ypos = y - (state->glyph_map.size[c][1]
					  - state->glyph_map.bearing[c][1]) * scale;

			/* if (!FT_Get_Kerning(face, */
			/* 		    last, c, */
			/* 		    FT_KERNING_DEFAULT, */
			/* 		    &kerning)) */
			/* {                              */
			/* 	xpos += (kerning.x >> 6) * scale; */
			/* } */

			// freetype only supports an old / barely used
			// kerning info
			// probably better to just not use it
			// or look for a different library

			f32 *tc = state->glyph_map._texcoords[c];
			f32 w = state->glyph_map.size[c][0] * scale;
			f32 h = state->glyph_map.size[c][1] * scale;

			f32 vertices[6][4] =
				{
					{xpos  , ypos+h, tc[0], 0.0},
					{xpos  , ypos  , tc[0], tc[2]},
					{xpos+w, ypos  , tc[1], tc[2]},
					{xpos  , ypos+h, tc[0], 0.0},
					{xpos+w, ypos  , tc[1], tc[2]},
					{xpos+w, ypos+h, tc[1], 0.0}
				};
                        memcpy(vbd + printed_characters, vertices, sizeof(vertices));

			/* memcpy(PTROFFSET(VertexBufferData, printed_characters * sizeof(vertices)), */
			/*        vertices, sizeof(vertices)); */
			x += (state->glyph_map.advance[c] >> 6) * scale;
			printed_characters += 1;
		}
		else
		{
			x = start_x;
			// we dont actually push data for a character here
			// but we cant just decrement the character count
// x >> 6 <=> x / 64
			y -= (state->glyph_map.height >> 6) * scale;
		}

	}


	glUnmapBuffer(GL_ARRAY_BUFFER);

	//TODO: check why transform doesnt work
	//TODO: draw black outlines instead of the BLEND thing
	glUniform3f(transformPos, 0.0, 0.0, 1.0f + OutlineWidth);
	glUniform3f(colorPos, 0.0f, 0.0f, 0.0f);
	glDisable(GL_BLEND);
	glDrawArrays(GL_TRIANGLES, 0, printed_characters * 6);
	glUniform3f(colorPos, Color[0], Color[1], Color[2]);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glUniform3f(transformPos, 0.0, 0.0, 1.0);
	glDrawArrays(GL_TRIANGLES, 0, printed_characters * 6);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void string_release(string_state *State)
{
	glDeleteProgram(State->StringProgram.Id);
	glDeleteBuffers(1, &State->vbo);
	glDeleteVertexArrays(1, &State->vao);
	glDeleteTextures(1, &State->glyph_map.texture);
}


void RenderString(string_state *State,
		  renderer     *Renderer,
		  f32 StartX, f32 StartY, f32 Scale,
		  f32 const Color[3],
		  u32 NumChars, char const *String)
{
	f32 X = StartX;
	f32 Y = StartY;
       	for (u32 CurrentChar = 0; CurrentChar < NumChars && String[CurrentChar]; ++CurrentChar)
	{
		int Char = (int) String[CurrentChar];
                if ((char) Char != '\n')
		{
			f32 GlyphX = X + State->glyph_map.bearing[Char][0] * Scale;
			f32 GlyphY = Y - (State->glyph_map.size[Char][1]
					  - State->glyph_map.bearing[Char][1]) * Scale;

			f32 *TexCoords = State->glyph_map._texcoords[Char];
			f32 Width = State->glyph_map.size[Char][0] * Scale;
			f32 Height = State->glyph_map.size[Char][1] * Scale;
			
			vertex_data vertices[6] =
				{
					{{GlyphX, GlyphY+Height, 0.0},
					 {TexCoords[0], 0.0, 0.0},
					 {Color[0], Color[1], Color[2], 1.0f}},
					{{GlyphX, GlyphY, 0.0},
					 {TexCoords[0], TexCoords[2], 0.0},
					 {Color[0], Color[1], Color[2], 1.0f}},
					{{GlyphX+Width, GlyphY, 0.0},
					 {TexCoords[1], TexCoords[2], 0.0},
					 {Color[0], Color[1], Color[2], 1.0f}},
					{{GlyphX, GlyphY+Height, 0.0},
					 {TexCoords[0], 0.0, 0.0},
					 {Color[0], Color[1], Color[2], 1.0f}},
					{{GlyphX+Width, GlyphY, 0.0},
					 {TexCoords[1], TexCoords[2], 0.0},
					 {Color[0], Color[1], Color[2], 1.0f}},
					{{GlyphX+Width, GlyphY+Height, 0.0},
					 {TexCoords[1], 0.0, 0.0},
					 {Color[0], Color[1], Color[2], 1.0f}},
				};

			DrawTexturedTriangles(Renderer, 6, vertices, State->glyph_map.texture);

			X += (State->glyph_map.advance[Char] >> 6) * Scale;
		}
		else
		{
                        X = StartX;
			Y -= (State->glyph_map.height >> 6) * Scale;
		}

	}
}
