#include <opengl/opengl.h>

#include <stdio.h>

void check_errors(char const *prefix)
{
	GLenum error;

	while (GL_NO_ERROR != (error = glGetError()))
	{
		printf("%s: ERROR %d\n", prefix, error);
		switch (error)
		{
			break;case GL_INVALID_ENUM:puts("INVALID ENUM");
                        break;case GL_INVALID_VALUE: puts("INVALID VALUE");
			break;case GL_INVALID_OPERATION: puts("INVALID OPERATION");
			break;case GL_STACK_OVERFLOW: puts("STACK OVERFLOW");
			break;case GL_STACK_UNDERFLOW: puts("STACK UNDERFLOW");
			break;case GL_OUT_OF_MEMORY: puts("OUT OF MEMORY");
			break;case GL_INVALID_FRAMEBUFFER_OPERATION: puts("INVALID FRAMEBUFFER OPERATION");
			break;case GL_CONTEXT_LOST: puts("CONTEXT LOST");
			break;case GL_TABLE_TOO_LARGE: puts("TABLE TOO LARGE");
		}

	}
}
